-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Feb 19, 2016 at 02:38 PM
-- Server version: 10.1.9-MariaDB
-- PHP Version: 5.6.15

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `tu_bms`
--

-- --------------------------------------------------------

--
-- Table structure for table `hc_categories`
--

CREATE TABLE `hc_categories` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `post_type` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `object_type` varchar(45) COLLATE utf8_unicode_ci DEFAULT '',
  `is_tag` tinyint(1) DEFAULT '0',
  `parent_id` int(11) DEFAULT NULL,
  `created_date` int(11) DEFAULT NULL,
  `modified_date` int(11) DEFAULT NULL,
  `author_id` int(11) DEFAULT NULL,
  `is_disabled` tinyint(1) DEFAULT '0',
  `is_deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `hc_categories`
--

INSERT INTO `hc_categories` (`id`, `title`, `slug`, `description`, `post_type`, `object_type`, `is_tag`, `parent_id`, `created_date`, `modified_date`, `author_id`, `is_disabled`, `is_deleted`) VALUES
(1, 'Buổi Ăn Sáng', NULL, '', 'mon-ngon', 'category', 0, 0, 1443192493, 1443192493, 1, 0, 0),
(2, 'Buổi Trưa', NULL, '', 'mon-ngon', 'category', 0, 0, 1443192493, 1443192493, 1, 0, 0),
(3, 'Buổi Tối', NULL, '', 'mon-ngon', 'category', 0, 0, 1443192493, 1443192493, 1, 0, 0),
(4, 'Món Ăn Khác', NULL, '', 'mon-ngon', 'category', 0, 0, 1443192493, 1443192493, 1, 0, 0),
(5, 'Thức Uống Không Cồn', NULL, '', 'mon-ngon', 'category', 0, 0, 1443192493, 1443192493, 1, 0, 0),
(6, 'Thức Uống Có Cồn', NULL, '', 'mon-ngon', 'category', 0, 0, 1443192493, 1443192493, 1, 0, 0),
(7, 'Rượu Vang', NULL, '', 'dac-san', 'category', 0, 0, 1443192493, 1443192493, 1, 0, 0),
(8, 'Trái Cây', NULL, '', 'dac-san', 'category', 0, 0, 1443192493, 1443192493, 1, 0, 0),
(9, 'Đặc sản Mứt', NULL, '', 'dac-san', 'category', 0, 0, 1443192493, 1443192493, 1, 0, 0),
(10, 'Quà lưu niệm', NULL, '', 'dac-san', 'category', 0, 0, 1443192493, 1443192493, 1, 0, 0),
(11, 'Trà & Coffee', NULL, '', 'dac-san', 'category', 0, 0, 1443192493, 1443192493, 1, 0, 0),
(12, 'Du Lịch Đà Lạt', NULL, '', 'post', 'tag', 0, 0, 1443276942, 1443276942, 1, 0, 0),
(13, 'Khách sạn Đà Lạt', NULL, '', 'post', 'tag', 0, 0, 1443276942, 1443276942, 1, 0, 0),
(14, 'Khách sạn giá rẻ', NULL, '', 'post', 'tag', 0, 0, 1443276942, 1443276942, 1, 0, 0),
(15, 'Đặt phòng Đà Lạt', NULL, '', 'post', 'tag', 0, 0, 1443276942, 1443276942, 1, 0, 0),
(16, 'Món ngon Đà Lạt', NULL, '', 'post', 'tag', 0, 0, 1443276942, 1443276942, 1, 0, 0),
(17, 'Đà Lạt', NULL, '', 'post', 'tag', 0, 0, 1443276942, 1443276942, 1, 0, 0),
(18, 'Du Lịch', NULL, '', 'post', 'tag', 0, 0, 1443276942, 1443276942, 1, 0, 0),
(19, 'Thông tin du lịch Đà Lạt', NULL, '', 'post', 'tag', 0, 0, 1443276942, 1443276942, 1, 0, 0),
(20, 'Tour Đà Lạt', NULL, '', 'post', 'tag', 0, 0, 1443276942, 1443276942, 1, 0, 0),
(21, 'Đặc sản Đà Lạt', NULL, '', 'post', 'tag', 0, 0, 1443276942, 1443276942, 1, 0, 0),
(22, 'Kinh nghiệm du lịch Đà Lạt', NULL, '', 'post', 'tag', 0, 0, 1443276942, 1443276942, 1, 0, 0),
(23, 'Du Lịch Đà Lạt', NULL, '', 'tin-tuc', 'tag', 0, 0, 1443276942, 1443276942, 1, 0, 0),
(24, 'Khách sạn Đà Lạt', NULL, '', 'tin-tuc', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(25, 'Khách sạn giá rẻ', NULL, '', 'tin-tuc', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(26, 'Đặt phòng Đà Lạt', NULL, '', 'tin-tuc', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(27, 'Món ngon Đà Lạt', NULL, '', 'tin-tuc', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(28, 'Đà Lạt', NULL, '', 'tin-tuc', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(29, 'Du Lịch', NULL, '', 'tin-tuc', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(30, 'Thông tin du lịch Đà Lạt', NULL, '', 'tin-tuc', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(31, 'Tour Đà Lạt', NULL, '', 'tin-tuc', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(32, 'Đặc sản Đà Lạt', NULL, '', 'tin-tuc', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(33, 'Kinh nghiệm du lịch Đà Lạt', NULL, '', 'tin-tuc', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(34, 'Du Lịch Đà Lạt', NULL, '', 'kinh-nghiem', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(35, 'Khách sạn Đà Lạt', NULL, '', 'kinh-nghiem', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(36, 'Khách sạn giá rẻ', NULL, '', 'kinh-nghiem', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(37, 'Đặt phòng Đà Lạt', NULL, '', 'kinh-nghiem', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(38, 'Món ngon Đà Lạt', NULL, '', 'kinh-nghiem', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(39, 'Đà Lạt', NULL, '', 'kinh-nghiem', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(40, 'Du Lịch', NULL, '', 'kinh-nghiem', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(41, 'Thông tin du lịch Đà Lạt', NULL, '', 'kinh-nghiem', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(42, 'Tour Đà Lạt', NULL, '', 'kinh-nghiem', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(43, 'Đặc sản Đà Lạt', NULL, '', 'kinh-nghiem', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(44, 'Kinh nghiệm du lịch Đà Lạt', NULL, '', 'kinh-nghiem', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(45, 'Du Lịch Đà Lạt', NULL, '', 'dia-diem', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(46, 'Khách sạn Đà Lạt', NULL, '', 'dia-diem', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(47, 'Khách sạn giá rẻ', NULL, '', 'dia-diem', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(48, 'Đặt phòng Đà Lạt', NULL, '', 'dia-diem', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(49, 'Món ngon Đà Lạt', NULL, '', 'dia-diem', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(50, 'Đà Lạt', NULL, '', 'dia-diem', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(51, 'Du Lịch', NULL, '', 'dia-diem', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(52, 'Thông tin du lịch Đà Lạt', NULL, '', 'dia-diem', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(53, 'Tour Đà Lạt', NULL, '', 'dia-diem', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(54, 'Đặc sản Đà Lạt', NULL, '', 'dia-diem', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(55, 'Kinh nghiệm du lịch Đà Lạt', NULL, '', 'dia-diem', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(56, 'Du Lịch Đà Lạt', NULL, '', 'tour', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(57, 'Khách sạn Đà Lạt', NULL, '', 'tour', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(58, 'Khách sạn giá rẻ', NULL, '', 'tour', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(59, 'Đặt phòng Đà Lạt', NULL, '', 'tour', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(60, 'Món ngon Đà Lạt', NULL, '', 'tour', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(61, 'Đà Lạt', NULL, '', 'tour', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(62, 'Du Lịch', NULL, '', 'tour', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(63, 'Thông tin du lịch Đà Lạt', NULL, '', 'tour', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(64, 'Tour Đà Lạt', NULL, '', 'tour', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(65, 'Đặc sản Đà Lạt', NULL, '', 'tour', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(66, 'Kinh nghiệm du lịch Đà Lạt', NULL, '', 'tour', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(67, 'Du Lịch Đà Lạt', NULL, '', 'mon-ngon', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(68, 'Khách sạn Đà Lạt', NULL, '', 'mon-ngon', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(69, 'Khách sạn giá rẻ', NULL, '', 'mon-ngon', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(70, 'Đặt phòng Đà Lạt', NULL, '', 'mon-ngon', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(71, 'Món ngon Đà Lạt', NULL, '', 'mon-ngon', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(72, 'Đà Lạt', NULL, '', 'mon-ngon', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(73, 'Du Lịch', NULL, '', 'mon-ngon', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(74, 'Thông tin du lịch Đà Lạt', NULL, '', 'mon-ngon', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(75, 'Tour Đà Lạt', NULL, '', 'mon-ngon', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(76, 'Đặc sản Đà Lạt', NULL, '', 'mon-ngon', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(77, 'Kinh nghiệm du lịch Đà Lạt', NULL, '', 'mon-ngon', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(78, 'Du Lịch Đà Lạt', NULL, '', 'dac-san', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(79, 'Khách sạn Đà Lạt', NULL, '', 'dac-san', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(80, 'Khách sạn giá rẻ', NULL, '', 'dac-san', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(81, 'Đặt phòng Đà Lạt', NULL, '', 'dac-san', 'tag', 0, 0, 1443276944, 1443276944, 1, 0, 0),
(82, 'Món ngon Đà Lạt', NULL, '', 'dac-san', 'tag', 0, 0, 1443276944, 1443276944, 1, 0, 0),
(83, 'Đà Lạt', NULL, '', 'dac-san', 'tag', 0, 0, 1443276944, 1443276944, 1, 0, 0),
(84, 'Du Lịch', NULL, '', 'dac-san', 'tag', 0, 0, 1443276944, 1443276944, 1, 0, 0),
(85, 'Thông tin du lịch Đà Lạt', NULL, '', 'dac-san', 'tag', 0, 0, 1443276944, 1443276944, 1, 0, 0),
(86, 'Tour Đà Lạt', NULL, '', 'dac-san', 'tag', 0, 0, 1443276944, 1443276944, 1, 0, 0),
(87, 'Đặc sản Đà Lạt', NULL, '', 'dac-san', 'tag', 0, 0, 1443276944, 1443276944, 1, 0, 0),
(88, 'Kinh nghiệm du lịch Đà Lạt', NULL, '', 'dac-san', 'tag', 0, 0, 1443276944, 1443276944, 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `hc_delivery_addresses`
--

CREATE TABLE `hc_delivery_addresses` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postal_code` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_home` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_primary` tinyint(1) DEFAULT '0',
  `created_date` int(11) DEFAULT NULL,
  `modified_date` int(11) DEFAULT NULL,
  `author_id` int(11) DEFAULT NULL,
  `is_disabled` tinyint(1) DEFAULT '0',
  `is_deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `hc_logs`
--

CREATE TABLE `hc_logs` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `old_data` longtext COLLATE utf8_unicode_ci,
  `new_data` longtext COLLATE utf8_unicode_ci,
  `object_type` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `object_id` int(11) DEFAULT NULL,
  `created_date` int(11) DEFAULT NULL,
  `modified_date` int(11) DEFAULT NULL,
  `author_id` int(11) DEFAULT NULL,
  `is_disabled` tinyint(1) DEFAULT '0',
  `is_deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `hc_migrations`
--

CREATE TABLE `hc_migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `hc_migrations`
--

INSERT INTO `hc_migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_01_30_102447_user', 2),
('2016_01_30_125944_reset', 3);

-- --------------------------------------------------------

--
-- Table structure for table `hc_options`
--

CREATE TABLE `hc_options` (
  `option_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `option_value` longtext COLLATE utf8_unicode_ci,
  `is_used` tinyint(1) NOT NULL DEFAULT '1',
  `created_date` int(11) DEFAULT '0',
  `modified_date` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `hc_password_resets`
--

CREATE TABLE `hc_password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `hc_posts`
--

CREATE TABLE `hc_posts` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `price` decimal(18,2) DEFAULT '0.00',
  `quantity` int(11) DEFAULT '0',
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `no_view` int(11) DEFAULT '0',
  `thumbnail` longtext COLLATE utf8_unicode_ci,
  `object_type` varchar(45) COLLATE utf8_unicode_ci DEFAULT '',
  `category_id` int(11) DEFAULT '0',
  `status` varchar(20) COLLATE utf8_unicode_ci DEFAULT 'draft',
  `parent_id` int(11) DEFAULT '0',
  `author_id` int(11) DEFAULT NULL,
  `is_disabled` tinyint(1) DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `is_deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `hc_posts`
--

INSERT INTO `hc_posts` (`id`, `title`, `slug`, `excerpt`, `description`, `price`, `quantity`, `address`, `city`, `email`, `phone`, `no_view`, `thumbnail`, `object_type`, `category_id`, `status`, `parent_id`, `author_id`, `is_disabled`, `created_at`, `updated_at`, `is_deleted`) VALUES
(1, 'Nguyễn Văn Tuấn', 'nguyen-van-tuan', 'This is demo 12', '', '0.00', 0, NULL, NULL, NULL, NULL, 0, NULL, 'post', 0, 'draft', 0, NULL, 0, '2016-02-06 00:00:00', '2016-02-06 00:00:00', 0),
(6, 'ge', 'ge', 'ge', '', '0.00', 0, NULL, NULL, NULL, NULL, 0, 'hinh-anh-vinamilk-day-manh-dau-tu-ra-nuoc-ngoai.jpg', 'post', 0, 'draft', 0, NULL, 0, '2016-02-09 00:00:00', '2016-02-09 00:00:00', 0),
(7, 'adasdfasdf', NULL, NULL, NULL, '0.00', 0, NULL, NULL, NULL, NULL, 0, NULL, '', 0, 'draft', 0, NULL, 0, '2016-02-10 07:48:59', '2016-02-10 07:48:59', 0),
(8, 'adasdfasdf', NULL, NULL, NULL, '0.00', 0, NULL, NULL, NULL, NULL, 0, NULL, '', 0, 'draft', 0, NULL, 0, '2016-02-10 07:49:46', '2016-02-10 07:49:46', 0),
(14, 'tu', 'tu', 'tu', '', '0.00', 0, NULL, NULL, NULL, NULL, 0, '', 'post', 0, 'draft', 0, NULL, 0, '2016-02-10 08:03:13', '2016-02-10 08:03:13', 0),
(15, 'tn', 'tn', 'tn', '', '0.00', 0, NULL, NULL, NULL, NULL, 0, '', 'post', 0, 'draft', 0, NULL, 0, '2016-02-10 15:10:48', '2016-02-10 15:10:48', 0);

-- --------------------------------------------------------

--
-- Table structure for table `hc_post_categories`
--

CREATE TABLE `hc_post_categories` (
  `post_id` int(11) NOT NULL DEFAULT '0',
  `category_id` int(11) NOT NULL DEFAULT '0',
  `created_date` int(11) NOT NULL DEFAULT '0',
  `modified_date` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `hc_post_categories`
--

INSERT INTO `hc_post_categories` (`post_id`, `category_id`, `created_date`, `modified_date`) VALUES
(3, 1, 1443515980, 1443515980),
(3, 2, 1443515981, 1443515981),
(3, 3, 1443515981, 1443515981),
(3, 4, 1443515981, 1443515981),
(4, 7, 1443279161, 1443279161),
(4, 10, 1443279161, 1443279161),
(14, 45, 1443276954, 1443276954),
(14, 48, 1443276954, 1443276954),
(14, 50, 1443276954, 1443276954),
(14, 51, 1443276954, 1443276954),
(14, 52, 1443276954, 1443276954),
(14, 54, 1443276954, 1443276954),
(14, 55, 1443276955, 1443276955),
(15, 3, 1443517473, 1443517473),
(15, 4, 1443517473, 1443517473),
(16, 2, 1443515828, 1443515828),
(16, 3, 1443515828, 1443515828),
(16, 4, 1443515828, 1443515828),
(17, 8, 1443278862, 1443278862),
(17, 10, 1443278862, 1443278862),
(17, 78, 1443278862, 1443278862),
(17, 81, 1443278862, 1443278862),
(17, 82, 1443278862, 1443278862),
(17, 84, 1443278862, 1443278862),
(17, 85, 1443278862, 1443278862),
(17, 87, 1443278862, 1443278862),
(17, 88, 1443278862, 1443278862),
(18, 8, 1443278968, 1443278968),
(18, 9, 1443278968, 1443278968),
(18, 10, 1443278968, 1443278968),
(18, 11, 1443278968, 1443278968),
(18, 80, 1443278968, 1443278968),
(18, 83, 1443278968, 1443278968),
(18, 85, 1443278968, 1443278968),
(18, 87, 1443278968, 1443278968),
(18, 88, 1443278968, 1443278968),
(19, 28, 1443510590, 1443510590);

-- --------------------------------------------------------

--
-- Table structure for table `hc_post_metas`
--

CREATE TABLE `hc_post_metas` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `meta_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_value` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `hc_post_metas`
--

INSERT INTO `hc_post_metas` (`id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'price', '1.500.000 VND'),
(2, 1, 'time', '1 ngày'),
(3, 2, 'price', '2.500.000 VND'),
(4, 2, 'time', '2 ngày'),
(5, 3, 'address', '254 Phan Đình Phùng, Tp. Đà Lạt'),
(6, 3, 'price', '30.000 VND 1 phần'),
(7, 4, 'address', 'Chợ Đà Lạt, Tp. Đà Lạt'),
(8, 4, 'price', '500.00 - 10.000.000 VND'),
(9, 5, 'address', 'thác Tam Hợp - Đà Lạt'),
(10, 11, 'address', 'thôn Xuân Trường - thành phố Đà Lạt'),
(11, 12, 'address', 'Windmills Coffee Đặng Dung - Đà Lạt'),
(12, 13, 'address', ' Làng Biệt thự Osaka Đà Lạt'),
(13, 14, 'address', 'Hồ Xuân Hương - Đà Lạt'),
(14, 15, 'address', ' Khu Ăn Vặt Chợ Đà Lạt - Chợ Đà Lạt, Tp. Đà Lạt '),
(15, 15, 'price', '8k hoặc 10k một cái'),
(16, 16, 'address', 'khu Hòa Bình gần chợ Đà Lạt'),
(17, 16, 'price', '4 người ăn 80k'),
(18, 17, 'address', 'Chợ Đà Lạt, Tp. Đà Lạt'),
(19, 17, 'price', ' 40.000đ/ kg'),
(20, 18, 'address', 'Chợ Đà Lạt, Tp. Đà Lạt'),
(21, 18, 'price', ' 80.000đ/ hộp 170gr');

-- --------------------------------------------------------

--
-- Table structure for table `hc_tokens`
--

CREATE TABLE `hc_tokens` (
  `id` bigint(20) NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_date` int(11) DEFAULT NULL,
  `expired_date` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0',
  `device_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `hc_users`
--

CREATE TABLE `hc_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `phone` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `thumbnail` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `hc_users`
--

INSERT INTO `hc_users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `phone`, `role`, `status`, `thumbnail`) VALUES
(1, 'Nguyễn Quốc Tú', 'tunguyenq@gmail.com', '$2y$10$UQlphgzncWLdNiTGfBOI1.5R5.uMeD3cUsHQ8SMidgsl0QdpQn46e', 'WW4DJlkR3HcyMMD5AU0Y9HUnaxqQVfv8Bpcw4hLxQTOChg3EUex7K7jqMLhB', '2016-02-15 12:09:28', '2016-02-15 12:09:28', '01693248887', 'admin', 'deactive', 'tunguyenq@gmail.com.jpg'),
(6, 'Nguyễn Minh Châu', 'minhchau@gmail.com', '$2y$10$PL4eSnbuUif4CdNJl6J86OBxtyBLnh1SECQJfBKuc04rOKXgHzT2K', NULL, '2016-02-11 06:14:41', '2016-02-11 06:14:41', '89234234', 'admin', 'active', ''),
(7, 'User TN', 'tunguyengoo@gmail.com', '$2y$10$42Y7IacpnbnyWj1M9VpzaOuwbaiEPRPYWJUPYcDPNY1lMFYdL4HSm', NULL, '2016-02-12 03:58:03', '2016-02-12 03:58:03', '', 'user', 'active', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `hc_categories`
--
ALTER TABLE `hc_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hc_delivery_addresses`
--
ALTER TABLE `hc_delivery_addresses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hc_logs`
--
ALTER TABLE `hc_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hc_options`
--
ALTER TABLE `hc_options`
  ADD PRIMARY KEY (`option_name`);

--
-- Indexes for table `hc_password_resets`
--
ALTER TABLE `hc_password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `hc_posts`
--
ALTER TABLE `hc_posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hc_post_categories`
--
ALTER TABLE `hc_post_categories`
  ADD PRIMARY KEY (`post_id`,`category_id`);

--
-- Indexes for table `hc_post_metas`
--
ALTER TABLE `hc_post_metas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hc_tokens`
--
ALTER TABLE `hc_tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hc_users`
--
ALTER TABLE `hc_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `hc_categories`
--
ALTER TABLE `hc_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;
--
-- AUTO_INCREMENT for table `hc_delivery_addresses`
--
ALTER TABLE `hc_delivery_addresses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `hc_logs`
--
ALTER TABLE `hc_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `hc_posts`
--
ALTER TABLE `hc_posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=16;
--
-- AUTO_INCREMENT for table `hc_post_metas`
--
ALTER TABLE `hc_post_metas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `hc_tokens`
--
ALTER TABLE `hc_tokens`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `hc_users`
--
ALTER TABLE `hc_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
