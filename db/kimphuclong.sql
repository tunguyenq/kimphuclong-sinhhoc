-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Aug 26, 2016 at 09:29 AM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.23

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `kimphuclong`
--

-- --------------------------------------------------------

--
-- Table structure for table `hc_categories`
--

CREATE TABLE `hc_categories` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `post_type` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `object_type` varchar(45) COLLATE utf8_unicode_ci DEFAULT '',
  `is_tag` tinyint(1) DEFAULT '0',
  `parent_id` int(11) DEFAULT NULL,
  `created_date` int(11) DEFAULT NULL,
  `modified_date` int(11) DEFAULT NULL,
  `author_id` int(11) DEFAULT NULL,
  `is_disabled` tinyint(1) DEFAULT '0',
  `is_deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `hc_categories`
--

INSERT INTO `hc_categories` (`id`, `title`, `slug`, `description`, `post_type`, `object_type`, `is_tag`, `parent_id`, `created_date`, `modified_date`, `author_id`, `is_disabled`, `is_deleted`) VALUES
(6, 'Khử mùi công nghiệp', 'khu-mui-cong-nghiep', '', 'mon-ngon', 'category', 0, 0, 1443192493, 1443192493, 1, 0, 0),
(7, 'Khử mùi nông nghiệp', 'khu-mui-nong-nghiep', '', 'dac-san', 'category', 0, 0, 1443192493, 1443192493, 1, 0, 0),
(8, 'Khử mùi văn phòng', 'khu-mui-van-phong', '', 'dac-san', 'category', 0, 0, 1443192493, 1443192493, 1, 0, 0),
(12, 'Du Lịch Đà Lạt', NULL, '', 'post', 'tag', 0, 0, 1443276942, 1443276942, 1, 0, 0),
(13, 'Khách sạn Đà Lạt', NULL, '', 'post', 'tag', 0, 0, 1443276942, 1443276942, 1, 0, 0),
(14, 'Khách sạn giá rẻ', NULL, '', 'post', 'tag', 0, 0, 1443276942, 1443276942, 1, 0, 0),
(15, 'Đặt phòng Đà Lạt', NULL, '', 'post', 'tag', 0, 0, 1443276942, 1443276942, 1, 0, 0),
(16, 'Món ngon Đà Lạt', NULL, '', 'post', 'tag', 0, 0, 1443276942, 1443276942, 1, 0, 0),
(17, 'Đà Lạt', NULL, '', 'post', 'tag', 0, 0, 1443276942, 1443276942, 1, 0, 0),
(18, 'Du Lịch', NULL, '', 'post', 'tag', 0, 0, 1443276942, 1443276942, 1, 0, 0),
(19, 'Thông tin du lịch Đà Lạt', NULL, '', 'post', 'tag', 0, 0, 1443276942, 1443276942, 1, 0, 0),
(20, 'Tour Đà Lạt', NULL, '', 'post', 'tag', 0, 0, 1443276942, 1443276942, 1, 0, 0),
(21, 'Đặc sản Đà Lạt', NULL, '', 'post', 'tag', 0, 0, 1443276942, 1443276942, 1, 0, 0),
(22, 'Kinh nghiệm du lịch Đà Lạt', NULL, '', 'post', 'tag', 0, 0, 1443276942, 1443276942, 1, 0, 0),
(23, 'Du Lịch Đà Lạt', NULL, '', 'tin-tuc', 'tag', 0, 0, 1443276942, 1443276942, 1, 0, 0),
(24, 'Khách sạn Đà Lạt', NULL, '', 'tin-tuc', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(25, 'Khách sạn giá rẻ', NULL, '', 'tin-tuc', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(26, 'Đặt phòng Đà Lạt', NULL, '', 'tin-tuc', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(27, 'Món ngon Đà Lạt', NULL, '', 'tin-tuc', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(28, 'Đà Lạt', NULL, '', 'tin-tuc', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(29, 'Du Lịch', NULL, '', 'tin-tuc', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(30, 'Thông tin du lịch Đà Lạt', NULL, '', 'tin-tuc', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(31, 'Tour Đà Lạt', NULL, '', 'tin-tuc', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(32, 'Đặc sản Đà Lạt', NULL, '', 'tin-tuc', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(33, 'Kinh nghiệm du lịch Đà Lạt', NULL, '', 'tin-tuc', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(34, 'Du Lịch Đà Lạt', NULL, '', 'kinh-nghiem', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(35, 'Khách sạn Đà Lạt', NULL, '', 'kinh-nghiem', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(36, 'Khách sạn giá rẻ', NULL, '', 'kinh-nghiem', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(37, 'Đặt phòng Đà Lạt', NULL, '', 'kinh-nghiem', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(38, 'Món ngon Đà Lạt', NULL, '', 'kinh-nghiem', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(39, 'Đà Lạt', NULL, '', 'kinh-nghiem', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(40, 'Du Lịch', NULL, '', 'kinh-nghiem', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(41, 'Thông tin du lịch Đà Lạt', NULL, '', 'kinh-nghiem', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(42, 'Tour Đà Lạt', NULL, '', 'kinh-nghiem', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(43, 'Đặc sản Đà Lạt', NULL, '', 'kinh-nghiem', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(44, 'Kinh nghiệm du lịch Đà Lạt', NULL, '', 'kinh-nghiem', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(45, 'Du Lịch Đà Lạt', NULL, '', 'dia-diem', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(46, 'Khách sạn Đà Lạt', NULL, '', 'dia-diem', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(47, 'Khách sạn giá rẻ', NULL, '', 'dia-diem', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(48, 'Đặt phòng Đà Lạt', NULL, '', 'dia-diem', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(49, 'Món ngon Đà Lạt', NULL, '', 'dia-diem', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(50, 'Đà Lạt', NULL, '', 'dia-diem', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(51, 'Du Lịch', NULL, '', 'dia-diem', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(52, 'Thông tin du lịch Đà Lạt', NULL, '', 'dia-diem', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(53, 'Tour Đà Lạt', NULL, '', 'dia-diem', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(54, 'Đặc sản Đà Lạt', NULL, '', 'dia-diem', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(55, 'Kinh nghiệm du lịch Đà Lạt', NULL, '', 'dia-diem', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(56, 'Du Lịch Đà Lạt', NULL, '', 'tour', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(57, 'Khách sạn Đà Lạt', NULL, '', 'tour', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(58, 'Khách sạn giá rẻ', NULL, '', 'tour', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(59, 'Đặt phòng Đà Lạt', NULL, '', 'tour', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(60, 'Món ngon Đà Lạt', NULL, '', 'tour', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(61, 'Đà Lạt', NULL, '', 'tour', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(62, 'Du Lịch', NULL, '', 'tour', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(63, 'Thông tin du lịch Đà Lạt', NULL, '', 'tour', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(64, 'Tour Đà Lạt', NULL, '', 'tour', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(65, 'Đặc sản Đà Lạt', NULL, '', 'tour', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(66, 'Kinh nghiệm du lịch Đà Lạt', NULL, '', 'tour', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(67, 'Du Lịch Đà Lạt', NULL, '', 'mon-ngon', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(68, 'Khách sạn Đà Lạt', NULL, '', 'mon-ngon', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(69, 'Khách sạn giá rẻ', NULL, '', 'mon-ngon', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(70, 'Đặt phòng Đà Lạt', NULL, '', 'mon-ngon', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(71, 'Món ngon Đà Lạt', NULL, '', 'mon-ngon', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(72, 'Đà Lạt', NULL, '', 'mon-ngon', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(73, 'Du Lịch', NULL, '', 'mon-ngon', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(74, 'Thông tin du lịch Đà Lạt', NULL, '', 'mon-ngon', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(75, 'Tour Đà Lạt', NULL, '', 'mon-ngon', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(76, 'Đặc sản Đà Lạt', NULL, '', 'mon-ngon', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(77, 'Kinh nghiệm du lịch Đà Lạt', NULL, '', 'mon-ngon', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(78, 'Du Lịch Đà Lạt', NULL, '', 'dac-san', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(79, 'Khách sạn Đà Lạt', NULL, '', 'dac-san', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(80, 'Khách sạn giá rẻ', NULL, '', 'dac-san', 'tag', 0, 0, 1443276943, 1443276943, 1, 0, 0),
(81, 'Đặt phòng Đà Lạt', NULL, '', 'dac-san', 'tag', 0, 0, 1443276944, 1443276944, 1, 0, 0),
(82, 'Món ngon Đà Lạt', NULL, '', 'dac-san', 'tag', 0, 0, 1443276944, 1443276944, 1, 0, 0),
(83, 'Đà Lạt', NULL, '', 'dac-san', 'tag', 0, 0, 1443276944, 1443276944, 1, 0, 0),
(84, 'Du Lịch', NULL, '', 'dac-san', 'tag', 0, 0, 1443276944, 1443276944, 1, 0, 0),
(85, 'Thông tin du lịch Đà Lạt', NULL, '', 'dac-san', 'tag', 0, 0, 1443276944, 1443276944, 1, 0, 0),
(86, 'Tour Đà Lạt', NULL, '', 'dac-san', 'tag', 0, 0, 1443276944, 1443276944, 1, 0, 0),
(87, 'Đặc sản Đà Lạt', NULL, '', 'dac-san', 'tag', 0, 0, 1443276944, 1443276944, 1, 0, 0),
(88, 'Kinh nghiệm du lịch Đà Lạt', NULL, '', 'dac-san', 'tag', 0, 0, 1443276944, 1443276944, 1, 0, 0);

-- --------------------------------------------------------

--
-- Table structure for table `hc_delivery_addresses`
--

CREATE TABLE `hc_delivery_addresses` (
  `id` int(11) NOT NULL,
  `user_id` int(11) DEFAULT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `state` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `country` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `postal_code` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone_home` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `is_primary` tinyint(1) DEFAULT '0',
  `created_date` int(11) DEFAULT NULL,
  `modified_date` int(11) DEFAULT NULL,
  `author_id` int(11) DEFAULT NULL,
  `is_disabled` tinyint(1) DEFAULT '0',
  `is_deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `hc_logs`
--

CREATE TABLE `hc_logs` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `old_data` longtext COLLATE utf8_unicode_ci,
  `new_data` longtext COLLATE utf8_unicode_ci,
  `object_type` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `object_id` int(11) DEFAULT NULL,
  `created_date` int(11) DEFAULT NULL,
  `modified_date` int(11) DEFAULT NULL,
  `author_id` int(11) DEFAULT NULL,
  `is_disabled` tinyint(1) DEFAULT '0',
  `is_deleted` tinyint(1) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `hc_migrations`
--

CREATE TABLE `hc_migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `hc_migrations`
--

INSERT INTO `hc_migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_01_30_102447_user', 2),
('2016_01_30_125944_reset', 3);

-- --------------------------------------------------------

--
-- Table structure for table `hc_options`
--

CREATE TABLE `hc_options` (
  `option_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `option_value` longtext COLLATE utf8_unicode_ci,
  `is_used` tinyint(1) NOT NULL DEFAULT '1',
  `created_date` int(11) DEFAULT '0',
  `modified_date` int(11) DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `hc_password_resets`
--

CREATE TABLE `hc_password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `hc_posts`
--

CREATE TABLE `hc_posts` (
  `id` int(11) NOT NULL,
  `title` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `slug` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `excerpt` text COLLATE utf8_unicode_ci,
  `description` text COLLATE utf8_unicode_ci,
  `price` decimal(18,2) DEFAULT '0.00',
  `quantity` int(11) DEFAULT '0',
  `address` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `city` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `phone` varchar(60) COLLATE utf8_unicode_ci DEFAULT NULL,
  `no_view` int(11) DEFAULT '0',
  `thumbnail` longtext COLLATE utf8_unicode_ci,
  `object_type` varchar(45) COLLATE utf8_unicode_ci DEFAULT '',
  `category_id` int(11) DEFAULT '0',
  `status` varchar(20) COLLATE utf8_unicode_ci DEFAULT 'draft',
  `parent_id` int(11) DEFAULT '0',
  `author_id` int(11) DEFAULT NULL,
  `is_disabled` tinyint(1) DEFAULT '0',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `is_deleted` tinyint(1) DEFAULT '0',
  `freeship` int(11) NOT NULL,
  `salevalue` int(11) NOT NULL,
  `saletype` varchar(20) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `hc_posts`
--

INSERT INTO `hc_posts` (`id`, `title`, `slug`, `excerpt`, `description`, `price`, `quantity`, `address`, `city`, `email`, `phone`, `no_view`, `thumbnail`, `object_type`, `category_id`, `status`, `parent_id`, `author_id`, `is_disabled`, `created_at`, `updated_at`, `is_deleted`, `freeship`, `salevalue`, `saletype`) VALUES
(1, 'Nguyễn Văn Tuấn', 'nguyen-van-tuan', 'This is demo 12', '', '0.00', 0, NULL, NULL, NULL, NULL, 0, NULL, 'post', 0, 'draft', 0, NULL, 0, '2016-02-06 00:00:00', '2016-02-06 00:00:00', 0, 0, 0, '0'),
(6, 'ge', 'ge', 'ge', '', '0.00', 0, NULL, NULL, NULL, NULL, 0, 'hinh-anh-vinamilk-day-manh-dau-tu-ra-nuoc-ngoai.jpg', 'post', 0, 'draft', 0, NULL, 0, '2016-02-09 00:00:00', '2016-02-09 00:00:00', 0, 0, 0, '0'),
(7, 'adasdfasdf', NULL, NULL, NULL, '0.00', 0, NULL, NULL, NULL, NULL, 0, NULL, '', 0, 'draft', 0, NULL, 0, '2016-02-10 07:48:59', '2016-02-10 07:48:59', 0, 0, 0, '0'),
(8, 'adasdfasdf', NULL, NULL, NULL, '0.00', 0, NULL, NULL, NULL, NULL, 0, NULL, '', 0, 'draft', 0, NULL, 0, '2016-02-10 07:49:46', '2016-02-10 07:49:46', 0, 0, 0, '0'),
(14, 'tu', 'tu', 'tu', '', '0.00', 0, NULL, NULL, NULL, NULL, 0, '', 'post', 0, 'draft', 0, NULL, 0, '2016-02-10 08:03:13', '2016-02-10 08:03:13', 0, 0, 0, '0'),
(15, 'tn', 'tn', 'tn', '', '0.00', 0, NULL, NULL, NULL, NULL, 0, '', 'post', 0, 'draft', 0, NULL, 0, '2016-02-10 15:10:48', '2016-02-10 15:10:48', 0, 0, 0, '0'),
(17, 'xxxx', 'xxxx', 'xxx', 'sdfa', '0.00', NULL, NULL, NULL, NULL, NULL, 0, '', 'video', 0, 'publish', 16, NULL, 0, '2016-08-14 15:17:40', '2016-08-16 22:23:06', 0, 0, 0, ''),
(22, 'slide1', 'slide1', 'slide', NULL, '0.00', 0, NULL, NULL, NULL, NULL, 0, '22813.jpg', 'slider', 0, 'draft', 0, NULL, 0, '2016-08-16 22:41:57', '2016-08-24 20:01:24', 0, 0, 0, ''),
(23, 'slide2', NULL, 'slide2', NULL, '0.00', 0, NULL, NULL, NULL, NULL, 0, 'small_22426.jpg', 'slider', 0, 'draft', 0, NULL, 0, '2016-08-18 20:35:57', '2016-08-23 22:59:35', 0, 0, 0, ''),
(24, 'Máy hút khử mùi không khí', 'may-hut-khu-mui-khong-khi', 'Khử mùi trong nhà kho, xưởng, ', '<p><span style="color:rgb(0, 0, 128); font-family:tahoma; font-size:14px">HSVN Global hiện nay l&agrave; thương hiệu uy t&iacute;n số 1 tại Việt nam , c&oacute; uy t&iacute;n l&acirc;u năm trong lĩnh vực cung cấp c&aacute;c loại&nbsp;</span><em><strong><a href="http://hsvn.com.vn/thiet-bi-khu-mui.html" style="color: rgb(119, 0, 17); text-decoration: none;"><span style="color:rgb(0, 0, 128)">m&aacute;y khử m&ugrave;i kh&ocirc;ng kh&iacute;</span></a></strong></em><span style="color:rgb(0, 0, 128); font-family:tahoma; font-size:14px">&nbsp;v&agrave; tư vấn giải ph&aacute;p khử m&ugrave;i kh&ocirc;ng kh&iacute; hiệu quả.</span><br />\r\n<br />\r\n<span style="color:rgb(0, 0, 128); font-family:tahoma; font-size:14px">Nh&agrave; kho, xưởng chế biến sản xuất của l&agrave; một trong những nơi thường xuy&ecirc;n c&oacute; nhiều m&ugrave;i. Trong những năm qua, HSVN Global đ&atilde; thực hiện rất nhiều dự &aacute;n thi c&ocirc;ng thiết kế v&agrave; lắp đặt&nbsp;</span><em><strong>m&aacute;y khử m&ugrave;i</strong></em><span style="color:rgb(0, 0, 128); font-family:tahoma; font-size:14px">&nbsp;cho nhiều xưởng tại khu c&ocirc;ng nghiệp nh&agrave; kho cho hộ gia đ&igrave;nh c&aacute; nh&acirc;n hoặc c&ocirc;ng ty tr&ecirc;n địa b&agrave;n H&agrave; Nội v&agrave; c&aacute;c tỉnh l&acirc;n cận.</span></p>\r\n', '0.00', 0, NULL, NULL, NULL, NULL, 0, '21896.jpg', 'product', 6, 'draft', 0, NULL, 0, '2016-08-23 22:37:39', '2016-08-24 20:39:00', 0, 0, 0, ''),
(25, 'máy khử mùi ozone', 'may-khu-mui-ozone', 'Khử mùi hôi khói thuốc lá', '<p>Xử l&yacute; m&ugrave;i h&ocirc;i của kh&oacute;i thuốc l&aacute; ho&agrave;n to&agrave;n kh&ocirc;ng đơn giản đặc biệt l&agrave; với m&ugrave;i h&ocirc;i của kh&oacute;i thuốc l&aacute; l&acirc;u ng&agrave;y b&aacute;m v&agrave;o c&aacute;c vật dụng như chăn m&agrave;n, quần &aacute;o, ga gối.<br />\r\nGiờ đ&acirc;y,&nbsp;<em><strong><a href="http://hsvn.com.vn/thiet-bi-khu-mui.html">m&aacute;y khử m&ugrave;i ozone</a></strong></em>&nbsp;sẽ gi&uacute;p bạn khử sạch ho&agrave;n to&agrave;n m&ugrave;i h&ocirc;i kh&oacute; chịu của thuốc l&aacute; trong thời gian ngắn nhất</p>\r\n\r\n<h2>Xử l&yacute; m&ugrave;i h&ocirc;i kh&oacute;i thuốc l&aacute; bằng những c&aacute;ch th&ocirc;ng thường</h2>\r\n\r\n<div>&nbsp;<em><strong>Kh&oacute;i thuốc l&aacute;&nbsp;</strong></em>kh&ocirc;ng chỉ g&acirc;y ảnh hưởng xấu tới sức khỏe của người h&uacute;t m&agrave; c&ograve;n ảnh hưởng tới những người xung quanh khi ngửi phải kh&oacute;i thuốc l&aacute;.&nbsp;</div>\r\n\r\n<p>Kh&oacute;i thuốc l&aacute; ra ngo&agrave;i kh&ocirc;ng kh&iacute; b&aacute;m tr&ecirc;n c&aacute;c vật dụng như r&egrave;m cửa, đệm, ga, chăn gối, thảm sẽ tạo ra m&ugrave;i h&ocirc;i rất kh&oacute; chịu, kh&ocirc;ng kh&iacute; trở n&ecirc;n ngột ngạt, kh&oacute;i thuốc l&aacute; lưu giữ l&acirc;u trong nh&agrave; sẽ g&acirc;y ra ảnh hưởng bất lợi đối với sức khỏe của c&aacute;c th&agrave;nh vi&ecirc;n trong gia đ&igrave;nh đặc việt l&agrave; phụ nữ c&oacute; thai, người gi&agrave; v&agrave; trẻ em.</p>\r\n', '0.00', 0, NULL, NULL, NULL, NULL, 0, 'xu ly mui hoi cua khoi thuoc la.jpg', 'product', 6, 'draft', 0, NULL, 0, '2016-08-23 22:43:16', '2016-08-23 22:43:16', 0, 0, 0, ''),
(26, 'máy khử mùi ozone 1', 'may-khu-mui-ozone-1', 'Xử lý sạch mùi trong nhà hàng bằng máy khử mùi ozone', '<p><span style="color:rgb(0, 0, 0); font-family:arial; font-size:14px"><span style="font-family:tahoma">Sản phẩm&nbsp;</span></span><em><a href="http://hsvn.com.vn/thiet-bi-khu-mui.html" style="color: rgb(119, 0, 17); text-decoration: none;"><span style="font-size:14px"><span style="font-family:tahoma"><strong>thiết bị&nbsp;</strong></span></span><span style="font-size:14px"><strong>m&aacute;y khử m&ugrave;i</strong>&nbsp;<strong>Ozone&nbsp;</strong></span></a></em><span style="color:rgb(0, 0, 0); font-family:arial; font-size:14px">hiện nay được ứng dụng nhiều trong c&aacute;c nh&agrave; h&agrave;ng, qu&aacute;n ăn để khử m&ugrave;i h&ocirc;i từ dầu mỡ, thức ăn, m&ugrave;i thuốc l&aacute;, m&ugrave;i điều h&ograve;a. Sử dụng m&aacute;y khử m&ugrave;i để xử l&yacute; m&ugrave;i trong nh&agrave; h&agrave;ng l&agrave; giải ph&aacute;p tối ưu nhất hiện nay v&igrave; vừa tiết kiệm được chi ph&iacute;, thời gian v&agrave; c&ocirc;ng sức.</span></p>\r\n', '0.00', 0, NULL, NULL, NULL, NULL, 0, 'quan net.jpg', 'product', 6, 'draft', 0, NULL, 0, '2016-08-23 22:45:35', '2016-08-23 23:02:47', 0, 0, 0, ''),
(27, 'máy khử mùi ozone 2', 'may-khu-mui-ozone-2', 'Xử lý khử mùi hôi rác thải ở chung cư, khu đô thị', '<p><span style="color:rgb(0, 0, 128); font-family:arial; font-size:12px"><span style="font-size:14px"><span style="font-family:tahoma">D&acirc;n số tăng nhanh, sản xuất c&ocirc;ng nghiệp b&ugrave;ng nổ trong những năm gần đ&acirc;y khiến cho lượng r&aacute;c thải thải ra m&ocirc;i trường c&agrave;ng ng&agrave;y c&agrave;ng nhiều trong khi đ&oacute; tại Việt nam hệ thống&nbsp;<a href="http://hsvn.com.vn/khu-mui-khu-chua-rac-thai.html" style="color: rgb(119, 0, 17); text-decoration: none;"><strong><span style="color:rgb(0, 0, 128)">xử l&yacute; m&ugrave;i r&aacute;c thải</span></strong></a>&nbsp;chưa tương xứng n&ecirc;n hiệu quả xử l&yacute; r&aacute;c thải chưa cao.</span></span></span></p>\r\n\r\n<div style="color: rgb(0, 0, 0); font-family: Arial; font-size: 12px; line-height: normal; text-align: justify;">&nbsp;</div>\r\n\r\n<div style="color: rgb(0, 0, 0); font-family: Arial; font-size: 12px; line-height: normal; text-align: justify;"><span style="color:rgb(0, 0, 128)"><span style="font-size:14px"><span style="font-family:tahoma">Tại những khu chung cư, khu đ&ocirc; thị hoặc c&aacute;c xưởng sản xuất, r&aacute;c thải được tập trung th&agrave;nh đống trước khi mang đi xử l&yacute; như ch&ocirc;n, lấp hoặc đốt..., tuy nhi&ecirc;n trong thời gian chờ xử l&yacute; r&aacute;c thải thường bốc m&ugrave;i rất kh&oacute; chịu g&acirc;y ảnh hưởng kh&ocirc;ng tốt đến đời sống, sức khỏe v&agrave; mỹ quan đ&ocirc; thị đặc biệt l&agrave; khu chứa r&aacute;c thải tại c&aacute;c khu chung cư hoặc nh&agrave; chứa r&aacute;c của nh&agrave; m&aacute;y, xưởng sản xuất</span></span></span></div>\r\n', '0.00', 0, NULL, NULL, NULL, NULL, 0, 'khu chua rac tai cac chung cu.jpg', 'product', 6, 'draft', 0, NULL, 0, '2016-08-23 22:46:54', '2016-08-23 22:46:54', 0, 0, 0, ''),
(28, 'máy khử mùi ozone 3', 'may-khu-mui-ozone-3', 'Khử mùi trong phòng họp, văn phòng , máy khử mùi không khí', '<h3 style="text-align:justify"><span style="font-size:14px"><span style="font-family:tahoma"><span style="color:rgb(255, 102, 0)"><strong>V&igrave; sao trong văn ph&ograve;ng, ph&ograve;ng họp của bạn c&oacute; m&ugrave;i?</strong></span></span></span>&nbsp;</h3>\r\n\r\n<p><span style="color:rgb(0, 0, 0); font-family:arial; font-size:14px"><span style="font-family:tahoma"><span style="color:rgb(0, 0, 128)">Tại c&aacute;c văn ph&ograve;ng, ph&ograve;ng họp của c&aacute;c c&ocirc;ng ty hiện nay thường xuy&ecirc;n đ&oacute;ng cửa k&iacute;n gần như 24/24 n&ecirc;n kh&ocirc;ng kh&iacute; trong ph&ograve;ng rất b&iacute;, ngo&agrave;i ra c&ograve;n c&oacute; c&aacute;c tạp m&ugrave;i như m&ugrave;i thuốc l&aacute;, m&ugrave;i giầy d&eacute;p, m&ugrave;i cơ thể người, m&ugrave;i nước hoa, m&ugrave;i mỹ phẩm, m&ugrave;i t&agrave;i liệu s&aacute;ch b&aacute;o, hồ sơ, m&ugrave;i ẩm mốc của r&egrave;m cửa,nấm mốc ở tường thảm đặc biệt l&agrave; m&ugrave;i điều h&ograve;a m&aacute;y lạnh, ...tất cả những m&ugrave;i tr&ecirc;n quyện lại v&agrave; tồn tại loanh quanh trong ph&ograve;ng g&acirc;y ra m&ugrave;i kh&oacute; chịu</span>&nbsp;</span></span></p>\r\n\r\n<p><span style="color:rgb(0, 0, 0); font-family:arial; font-size:14px"><span style="font-family:tahoma"><span style="color:rgb(0, 0, 128)">Trong qu&aacute; tr&igrave;nh l&agrave;m việc, những nh&acirc;n vi&ecirc;n tại c&aacute;c văn ph&ograve;ng c&ocirc;ng sở tiếp x&uacute;c l&acirc;u d&agrave;i với hỗn hợp m&ugrave;i n&agrave;y sẽ c&oacute; khả năng bị ảnh hưởng đến sức khỏe</span>&nbsp;</span></span></p>\r\n\r\n<p><span style="color:rgb(0, 0, 0); font-family:arial; font-size:14px"><span style="font-family:tahoma"><span style="color:rgb(0, 0, 128)">Bạn muốn kh&ocirc;ng kh&iacute; trongvăn ph&ograve;ng, ph&ograve;ng họp của c&ocirc;ng ty trở th&ocirc;ng tho&aacute;ng v&agrave; kh&ocirc;ng c&oacute; m&ugrave;i?</span></span></span></p>\r\n', '0.00', 0, NULL, NULL, NULL, NULL, 0, 've sinh van phong khu mui hoi.jpg', 'product', 6, 'draft', 0, NULL, 0, '2016-08-23 22:48:25', '2016-08-23 22:48:25', 0, 0, 0, ''),
(29, 'máy khử mùi ozone 4', 'may-khu-mui-ozone-4', 'Máy khử mùi thuốc lá trong quán Cafe, Karaoke sạch 96%', '<p>Ng&agrave;y nay c&aacute;c&nbsp;<strong><a href="http://hsvn.com.vn/thiet-bi-khu-mui.html">thiết bị m&aacute;y khử m&ugrave;i&nbsp;</a></strong>được ứng dụng rất nhiều trong cuộc sống. Tại c&aacute;c qu&aacute;n Cafe hoặc qu&aacute;n Karaoke, m&aacute;y khử m&ugrave;i được sử dụng để h&uacute;t c&aacute;c loại m&ugrave;i h&ocirc;i kh&oacute; chịu như m&ugrave;i thuốc l&aacute;, m&ugrave;i đồ uống, m&ugrave;i mỹ phẩm, h&oacute;a chất, m&ugrave;i m&aacute;y lạnh. C&aacute;c m&ugrave;i h&ocirc;i bị khử sạch ho&agrave;n to&agrave;n, trả lại bầu kh&ocirc;ng kh&iacute; trong l&agrave;nh v&agrave; tho&aacute;ng m&aacute;t, kh&ocirc;ng c&oacute; m&ugrave;i.&nbsp;</p>\r\n\r\n<h2>Vấn đề m&ugrave;i trong qu&aacute;n cafe hoặc ph&ograve;ng Karaoke</h2>\r\n\r\n<p>L&agrave; những nơi c&ocirc;ng cộng, thường xuy&ecirc;n c&oacute; nhiều người n&ecirc;n những qu&aacute;n cafe hoặc Karaoke thường kh&ocirc;ng thể tr&aacute;nh khỏi c&oacute; những m&ugrave;i h&ocirc;i xuất hiện.<br />\r\n<br />\r\nChất lượng dịch vụ l&agrave; y&ecirc;u cầu số 1, tuy nhi&ecirc;n phong c&aacute;ch kh&ocirc;ng gian cũng l&agrave; một trong những điều quan trọng của một qu&aacute;n cafe hoặc Karaoke để tạo sự thu h&uacute;t với kh&aacute;ch h&agrave;ng&nbsp;</p>\r\n\r\n<p>Tuy nhi&ecirc;n với kh&ocirc;ng gian chật chội nơi đ&ocirc; thị hầu như kh&ocirc;ng gian tại c&aacute;c qu&aacute;n Cafe , karaoke hiện nay l&agrave; kh&ocirc;ng gian kh&eacute;p k&iacute;n v&igrave; diện t&iacute;ch qu&aacute; &iacute;t, chủ yếu l&agrave; kh&eacute;p k&iacute;n v&agrave; lưu th&ocirc;ng với kh&ocirc;ng kh&iacute; b&ecirc;n ngo&agrave;i bằng c&aacute;c thiết bị như điều h&ograve;a, m&aacute;y lạnh, quạt th&ocirc;ng gi&oacute;.&nbsp;</p>\r\n', '0.00', 0, NULL, NULL, NULL, NULL, 0, 'quan cafe.jpg', 'product', 6, 'draft', 0, NULL, 0, '2016-08-23 22:49:42', '2016-08-23 22:49:42', 0, 0, 0, ''),
(30, 'máy khử mùi ozone 5', 'may-khu-mui-ozone-5', 'Khử mùi trong y tế, bệnh viện, máy khử mùi không khí', '<p><span style="color:rgb(0, 0, 128); font-family:tahoma; font-size:14px">Sản phẩm&nbsp;</span><em><strong><a href="http://hsvn.com.vn/thiet-bi-khu-mui.html" style="color: rgb(119, 0, 17); text-decoration: none;">thiết bị m&aacute;y khử m&ugrave;i</a></strong></em><span style="color:rgb(0, 0, 128); font-family:tahoma; font-size:14px">&nbsp;l&agrave; sản phẩm độc quyền chuy&ecirc;n khử m&ugrave;i kh&ocirc;ng kh&iacute; của c&ocirc;ng ty TNHH Ph&aacute;t triển c&ocirc;ng nghệ v&agrave; dịch vụ HSVN To&agrave;n cầu. Sản phẩm đ&atilde; được ứng dụng nhiều trong qu&aacute; tr&igrave;nh xử l&yacute; m&ugrave;i tại c&aacute;c&nbsp;</span><em>bệnh viện lớn</em><span style="color:rgb(0, 0, 128); font-family:tahoma; font-size:14px">&nbsp;tại H&agrave; Nội v&agrave; c&aacute;c tỉnh tr&ecirc;n to&agrave;n quốc</span></p>\r\n', '0.00', 0, NULL, NULL, NULL, NULL, 0, 'may khu mui trong benh vien.jpg', 'product', 6, 'draft', 0, NULL, 0, '2016-08-23 22:50:51', '2016-08-23 22:50:51', 0, 0, 0, ''),
(31, 'máy khử mùi ozone 6', 'may-khu-mui-ozone-6', 'Máy khử mùi trong quán net, tiệm game', '<p style="text-align:justify"><span style="color:rgb(0, 0, 128)"><span style="font-size:13px"><span style="font-family:tahoma"><span style="font-size:14px">Xử l&yacute; m&ugrave;i h&ocirc;i trong qu&aacute;n net, tiệm game thực sự l&agrave; một việc v&ocirc; c&ugrave;ng kh&oacute; khăn đối với những người l&agrave;m chủ hoặc quản l&yacute;. Sử dụng<em><strong><a href="http://hsvn.com.vn/thiet-bi-khu-mui.html" style="color: rgb(119, 0, 17); text-decoration: none;"><span style="color:rgb(0, 0, 128)">m&aacute;y khử m&ugrave;i ozone</span></a></strong></em>&nbsp;để khử m&ugrave;i trong qu&aacute;n net, tiệm game hiện nay đang l&agrave; phương ph&aacute;p khử m&ugrave;i hiệu quả nhất.</span></span></span></span></p>\r\n\r\n<p style="text-align:justify"><span style="color:rgb(255, 102, 0)"><span style="font-size:14px"><span style="font-family:tahoma"><em><strong>M&ugrave;i h&ocirc;i tại c&aacute;c qu&aacute;n net, tiệm game&nbsp;</strong></em></span></span></span>&nbsp;</p>\r\n\r\n<p><span style="color:rgb(0, 0, 128); font-family:arial; font-size:12px"><span style="font-size:14px"><span style="font-family:tahoma">Sẽ kh&ocirc;ng lạ g&igrave; khi bạn bước ch&acirc;n v&agrave;o qu&aacute;n n&eacute;t hoặc tiệm game v&agrave; bạn cảm thấy nhiều m&ugrave;i kh&oacute; chịu đặc biệt l&agrave; m&ugrave;i h&ocirc;i ngai ng&aacute;i của cơ thể người trộn lẫn c&ugrave;ng với m&ugrave;i nồng của kh&oacute;i thuốc l&aacute;. Thật l&agrave; kinh khủng !</span>&nbsp;</span></span><br />\r\n<span style="color:rgb(0, 0, 128); font-family:arial; font-size:12px"><span style="font-size:14px"><span style="font-family:tahoma">Tại c&aacute;c th&agrave;nh phố lớn ph&aacute;t triển internet, mỗi qu&aacute;n n&eacute;t th&igrave; hầu hết quy m&ocirc; cũng gần từ 30 -&gt;40 d&agrave;n m&aacute;y v&agrave; được kh&eacute;p k&iacute;n t&aacute;ch biệt với m&ocirc;i trường b&ecirc;n ngo&agrave;i, kh&ocirc;ng kh&iacute; lưu th&ocirc;ng trong ph&ograve;ng chỉ nhờ v&agrave;o m&aacute;y điều h&ograve;a</span></span></span></p>\r\n', '0.00', 0, NULL, NULL, NULL, NULL, 0, 'quan net.jpg', 'product', 6, 'draft', 0, NULL, 0, '2016-08-23 22:51:52', '2016-08-23 22:51:52', 0, 0, 0, ''),
(32, 'máy khử mùi ozone 7', 'may-khu-mui-ozone-7', 'Máy khử mùi, làm sạch không khí trong ô tô nhanh hiệu quả', '<p><span style="color:rgb(255, 0, 0); font-family:arial; font-size:12px"><span style="color:rgb(0, 0, 128)"><span style="font-size:16px"><span style="font-family:comic sans ms"><span style="font-size:14px"><span style="font-family:tahoma">Bạn đang t&igrave;m một loại&nbsp;<a href="http://hsvn.com.vn/khu-mui-o-to.html" style="color: rgb(119, 0, 17); text-decoration: none;"><strong><span style="color:rgb(0, 0, 128)">thiết bị khử m&ugrave;i tr&ecirc;n xe &ocirc; t&ocirc;</span></strong></a>&nbsp; hoặc c&aacute;ch l&agrave;m sạch m&ugrave;i xe &ocirc; t&ocirc; của bạn bởi v&igrave;:&nbsp;</span></span></span></span></span></span><br />\r\n<br />\r\n<span style="color:rgb(0, 0, 128); font-family:arial; font-size:12px"><span style="font-size:14px"><span style="font-family:tahoma"><img alt="Máy khử mùi  DrOzone Clean Pro" src="http://hsvn.com.vn/Images/image/M%C3%A1y%20ozone%20Drozone/Khu%20mui%20gia%20dinh/muiten.png" style="border:0px; height:16px; text-align:left; width:16px" />&nbsp;Xe bạn c&oacute; m&ugrave;i thuốc l&aacute; kh&oacute; chịu<br />\r\n<img alt="Máy khử mùi  DrOzone Clean Pro" src="http://hsvn.com.vn/Images/image/M%C3%A1y%20ozone%20Drozone/Khu%20mui%20gia%20dinh/muiten.png" style="border:0px; height:16px; text-align:left; width:16px" />&nbsp;M&ugrave;i nước tiểu khai của trẻ, m&ugrave;i thức ăn, nước uống, m&ugrave;i rượu<br />\r\n<img alt="Máy khử mùi  DrOzone Clean Pro" src="http://hsvn.com.vn/Images/image/M%C3%A1y%20ozone%20Drozone/Khu%20mui%20gia%20dinh/muiten.png" style="border:0px; height:16px; text-align:left; width:16px" />&nbsp;M&ugrave;i ẩm mốc từ thảm l&oacute;t ch&acirc;n hoặc chất liệu da bọc ghế ngồi tr&ecirc;n xe mới<br />\r\n<img alt="Máy khử mùi  DrOzone Clean Pro" src="http://hsvn.com.vn/Images/image/M%C3%A1y%20ozone%20Drozone/Khu%20mui%20gia%20dinh/muiten.png" style="border:0px; height:16px; text-align:left; width:16px" />&nbsp;M&ugrave;i người tr&ecirc;n xe<br />\r\n<img alt="Máy khử mùi  DrOzone Clean Pro" src="http://hsvn.com.vn/Images/image/M%C3%A1y%20ozone%20Drozone/Khu%20mui%20gia%20dinh/muiten.png" style="border:0px; height:16px; text-align:left; width:16px" />&nbsp;M&ugrave;i xăng hoặc m&ugrave;i điều h&ograve;a, m&ugrave;i hải sản,..</span></span></span><br />\r\n&nbsp;</p>\r\n', '0.00', 0, NULL, NULL, NULL, NULL, 0, 'may-lam-mat-nakami-ac-6000-dv-1160a-218.jpg', 'product', 6, 'draft', 0, NULL, 0, '2016-08-23 22:54:38', '2016-08-23 22:54:38', 0, 0, 0, ''),
(33, 'máy khử mùi ozone 8', 'may-khu-mui-ozone-8', 'Tư vấn lắp hệ thống thông gió làm mát cho tầng hầm', '<p><span style="color:rgb(0, 0, 128); font-family:tahoma; font-size:14px">Tầng hầm chung cư, nh&agrave; cao tầng lu&ocirc;n l&agrave; nơi b&iacute; kh&iacute; ch&iacute;nh v&igrave; thế trong qu&aacute; tr&igrave;nh x&acirc;y dựng th&igrave; một c&ocirc;ng đoạn kh&ocirc;ng thể thiếu được đ&oacute; l&agrave; x&acirc;y dựng&nbsp;</span><strong>hệ thống th&ocirc;ng gi&oacute; l&agrave;m m&aacute;t cho tầng hầm</strong><span style="color:rgb(0, 0, 128); font-family:tahoma; font-size:14px">. Bạn đang t&igrave;m đơn vị tư vấn thiết kế lắp đặt hệ thống quạt th&ocirc;ng gi&oacute; tầng hầm. H&atilde;y li&ecirc;n hệ với HSVN To&agrave;n Cầu để được tư vấn chuy&ecirc;n s&acirc;u.</span><br />\r\n&nbsp;</p>\r\n', '0.00', 0, NULL, NULL, NULL, NULL, 0, 'thong gio lam mat cho tang ham.jpg', 'product', 6, 'draft', 0, NULL, 0, '2016-08-23 22:56:01', '2016-08-23 22:56:01', 0, 0, 0, ''),
(34, 'slide3', NULL, 'slide3', NULL, '0.00', 0, NULL, NULL, NULL, NULL, 0, '13094.jpg', 'slider', 0, 'draft', 0, NULL, 0, '2016-08-23 23:00:02', '2016-08-24 19:53:06', 0, 0, 0, ''),
(35, 'slide4', NULL, 'slide4', NULL, '0.00', 0, NULL, NULL, NULL, NULL, 0, '03.jpg', 'slider', 0, 'draft', 0, NULL, 0, '2016-08-23 23:00:31', '2016-08-23 23:00:31', 0, 0, 0, ''),
(36, 'Liên hệ', 'lien-he', 'liên hệ', '', '0.00', 0, NULL, NULL, NULL, NULL, 0, '', 'page', 0, 'draft', 0, NULL, 0, '2016-08-23 23:46:13', '2016-08-23 23:46:13', 0, 0, 0, ''),
(37, 'Tuyển dụng', 'tuyen-dung', 'tuyển dụng', '<p>đ&acirc;y l&agrave; nội dung trang tuyển dụng</p>\r\n', '0.00', 0, NULL, NULL, NULL, NULL, 0, '', 'page', 0, 'draft', 0, NULL, 0, '2016-08-23 23:46:44', '2016-08-23 23:46:44', 0, 0, 0, ''),
(38, 'Giới thiệu', 'gioi-thieu', 'giới thiệu', '<p>đ&acirc;y l&agrave; trang igới thiệu</p>\r\n', '0.00', 0, NULL, NULL, NULL, NULL, 0, '', 'page', 0, 'draft', 0, NULL, 0, '2016-08-23 23:47:16', '2016-08-23 23:47:16', 0, 0, 0, '');

-- --------------------------------------------------------

--
-- Table structure for table `hc_post_categories`
--

CREATE TABLE `hc_post_categories` (
  `post_id` int(11) NOT NULL DEFAULT '0',
  `category_id` int(11) NOT NULL DEFAULT '0',
  `created_date` int(11) NOT NULL DEFAULT '0',
  `modified_date` int(11) NOT NULL DEFAULT '0'
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `hc_post_categories`
--

INSERT INTO `hc_post_categories` (`post_id`, `category_id`, `created_date`, `modified_date`) VALUES
(3, 1, 1443515980, 1443515980),
(3, 2, 1443515981, 1443515981),
(3, 3, 1443515981, 1443515981),
(3, 4, 1443515981, 1443515981),
(4, 7, 1443279161, 1443279161),
(4, 10, 1443279161, 1443279161),
(14, 45, 1443276954, 1443276954),
(14, 48, 1443276954, 1443276954),
(14, 50, 1443276954, 1443276954),
(14, 51, 1443276954, 1443276954),
(14, 52, 1443276954, 1443276954),
(14, 54, 1443276954, 1443276954),
(14, 55, 1443276955, 1443276955),
(15, 3, 1443517473, 1443517473),
(15, 4, 1443517473, 1443517473),
(16, 2, 1443515828, 1443515828),
(16, 3, 1443515828, 1443515828),
(16, 4, 1443515828, 1443515828),
(17, 8, 1443278862, 1443278862),
(17, 10, 1443278862, 1443278862),
(17, 78, 1443278862, 1443278862),
(17, 81, 1443278862, 1443278862),
(17, 82, 1443278862, 1443278862),
(17, 84, 1443278862, 1443278862),
(17, 85, 1443278862, 1443278862),
(17, 87, 1443278862, 1443278862),
(17, 88, 1443278862, 1443278862),
(18, 8, 1443278968, 1443278968),
(18, 9, 1443278968, 1443278968),
(18, 10, 1443278968, 1443278968),
(18, 11, 1443278968, 1443278968),
(18, 80, 1443278968, 1443278968),
(18, 83, 1443278968, 1443278968),
(18, 85, 1443278968, 1443278968),
(18, 87, 1443278968, 1443278968),
(18, 88, 1443278968, 1443278968),
(19, 28, 1443510590, 1443510590);

-- --------------------------------------------------------

--
-- Table structure for table `hc_post_metas`
--

CREATE TABLE `hc_post_metas` (
  `id` int(11) NOT NULL,
  `post_id` int(11) NOT NULL,
  `meta_key` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `meta_value` longtext COLLATE utf8_unicode_ci
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `hc_post_metas`
--

INSERT INTO `hc_post_metas` (`id`, `post_id`, `meta_key`, `meta_value`) VALUES
(1, 1, 'price', '1.500.000 VND'),
(2, 1, 'time', '1 ngày'),
(3, 2, 'price', '2.500.000 VND'),
(4, 2, 'time', '2 ngày'),
(5, 3, 'address', '254 Phan Đình Phùng, Tp. Đà Lạt'),
(6, 3, 'price', '30.000 VND 1 phần'),
(7, 4, 'address', 'Chợ Đà Lạt, Tp. Đà Lạt'),
(8, 4, 'price', '500.00 - 10.000.000 VND'),
(9, 5, 'address', 'thác Tam Hợp - Đà Lạt'),
(10, 11, 'address', 'thôn Xuân Trường - thành phố Đà Lạt'),
(11, 12, 'address', 'Windmills Coffee Đặng Dung - Đà Lạt'),
(12, 13, 'address', ' Làng Biệt thự Osaka Đà Lạt'),
(13, 14, 'address', 'Hồ Xuân Hương - Đà Lạt'),
(14, 15, 'address', ' Khu Ăn Vặt Chợ Đà Lạt - Chợ Đà Lạt, Tp. Đà Lạt '),
(15, 15, 'price', '8k hoặc 10k một cái'),
(16, 16, 'address', 'khu Hòa Bình gần chợ Đà Lạt'),
(17, 16, 'price', '4 người ăn 80k'),
(18, 17, 'address', 'Chợ Đà Lạt, Tp. Đà Lạt'),
(19, 17, 'price', ' 40.000đ/ kg'),
(20, 18, 'address', 'Chợ Đà Lạt, Tp. Đà Lạt'),
(21, 18, 'price', ' 80.000đ/ hộp 170gr');

-- --------------------------------------------------------

--
-- Table structure for table `hc_tokens`
--

CREATE TABLE `hc_tokens` (
  `id` bigint(20) NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `user_id` int(11) DEFAULT NULL,
  `created_date` int(11) DEFAULT NULL,
  `expired_date` int(11) DEFAULT NULL,
  `status` tinyint(4) DEFAULT '0',
  `device_id` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `hc_users`
--

CREATE TABLE `hc_users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(60) COLLATE utf8_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `phone` varchar(15) COLLATE utf8_unicode_ci NOT NULL,
  `role` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `status` varchar(128) COLLATE utf8_unicode_ci NOT NULL,
  `thumbnail` varchar(255) COLLATE utf8_unicode_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `hc_users`
--

INSERT INTO `hc_users` (`id`, `name`, `email`, `password`, `remember_token`, `created_at`, `updated_at`, `phone`, `role`, `status`, `thumbnail`) VALUES
(1, 'Nguyễn Quốc Tú', 'tunguyenq@gmail.com', '$2y$10$UQlphgzncWLdNiTGfBOI1.5R5.uMeD3cUsHQ8SMidgsl0QdpQn46e', 'WW4DJlkR3HcyMMD5AU0Y9HUnaxqQVfv8Bpcw4hLxQTOChg3EUex7K7jqMLhB', '2016-02-15 12:09:28', '2016-02-15 12:09:28', '01693248887', 'admin', 'deactive', 'tunguyenq@gmail.com.jpg'),
(6, 'Nguyễn Minh Châu', 'minhchau@gmail.com', '$2y$10$PL4eSnbuUif4CdNJl6J86OBxtyBLnh1SECQJfBKuc04rOKXgHzT2K', NULL, '2016-02-11 06:14:41', '2016-02-11 06:14:41', '89234234', 'admin', 'active', ''),
(7, 'User TN', 'tunguyengoo@gmail.com', '$2y$10$42Y7IacpnbnyWj1M9VpzaOuwbaiEPRPYWJUPYcDPNY1lMFYdL4HSm', NULL, '2016-02-12 03:58:03', '2016-02-12 03:58:03', '', 'user', 'active', ''),
(8, 'hoaiph026@gmail.com', 'hoaiph026@gmail.com', '$2y$10$MZVmXIr6rssLsHtW76oYb.XhkHUK2pa9eoYuIf3EL8uG7wcW/UzJu', NULL, '2016-08-16 16:39:49', '2016-08-16 16:39:49', 'hoaiph026@gmail', 'admin', '', '');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `hc_categories`
--
ALTER TABLE `hc_categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hc_delivery_addresses`
--
ALTER TABLE `hc_delivery_addresses`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hc_logs`
--
ALTER TABLE `hc_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hc_options`
--
ALTER TABLE `hc_options`
  ADD PRIMARY KEY (`option_name`);

--
-- Indexes for table `hc_password_resets`
--
ALTER TABLE `hc_password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `hc_posts`
--
ALTER TABLE `hc_posts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hc_post_categories`
--
ALTER TABLE `hc_post_categories`
  ADD PRIMARY KEY (`post_id`,`category_id`);

--
-- Indexes for table `hc_post_metas`
--
ALTER TABLE `hc_post_metas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hc_tokens`
--
ALTER TABLE `hc_tokens`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `hc_users`
--
ALTER TABLE `hc_users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `hc_categories`
--
ALTER TABLE `hc_categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=89;
--
-- AUTO_INCREMENT for table `hc_delivery_addresses`
--
ALTER TABLE `hc_delivery_addresses`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `hc_logs`
--
ALTER TABLE `hc_logs`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `hc_posts`
--
ALTER TABLE `hc_posts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;
--
-- AUTO_INCREMENT for table `hc_post_metas`
--
ALTER TABLE `hc_post_metas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `hc_tokens`
--
ALTER TABLE `hc_tokens`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `hc_users`
--
ALTER TABLE `hc_users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
