<div class="footer-widget">
    <div class="container">
        <div class="footer-widget-wrap">
            <div class="row">
                <div class="footer-widget-col col-md-3 col-sm-6">
                    <div class="widget widget_text">
                        <div class="textwidget">
                            <p>
                                <img src='{{URL::asset('public/media/images/logo.jpg')}}' alt="" width="80"/>
                                <br/><br/>
                                    Công ty sản phẩm môi trường
                                <br/>
                            </p>
                        </div>
                    </div>
                    <div class="widget social-widget">
                        <div class="social-widget-wrap social-widget-outlined">
                            <a href="#" title="Facebook" target="_blank">
                                <i class="fa fa-facebook facebook-bg-hover facebook-outlined"></i>
                            </a>
                            <a href="#" title="Google+" target="_blank">
                                <i class="fa fa-google-plus google-plus-bg-hover google-plus-outlined"></i>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="footer-widget-col col-md-3 col-sm-6">
                    <div class="widget widget-post-thumbnail hidden">
                        <h3 class="widget-title">
                            <span>Sản phẩm nổi bật</span>
                        </h3>
                        <ul class="posts-thumbnail-list">

                        </ul>
                    </div>
                </div>
                <div class="footer-widget-col col-md-3 col-sm-6">
                    <div class="widget tweets-widget">

                    </div>
                </div>
                <div class="footer-widget-col col-md-3 col-sm-6">
                    <div class="widget widget_text">
                        <h3 class="widget-title">
                            <span>Liên hệ</span>
                        </h3>
                        <div class="textwidget">
                            <p>Công ty TNHH thương mại đầu tư phát triển Kim Phúc Long</p>
                            <p>
                                <i class="fa fa-home"></i> 95/11/9 đường HT 45, phường Hiệp Thành, quận 12, Hồ chí minh<br><br/>
                                <i class="fa fa-phone"></i> Hotline: 098 6393160<br><br/>
                                <i class="fa fa-envelope"></i>
                                <a href="#"><i
                                            class="fa fa-envelope-o"></i><span><span class="__cf_email__"
                                                                                     data-cfemail="b5dcdbd3daf5d1dad8d4dcdb9bd6dad8">Email: info@kimphuclong.com.vn</span></span></a>
                            </p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<footer class="footer">
    <div class="footer-info">
        <div class="container">
            <div class="footer-info-wrap">
                <div class="row">
                    <div class="col-sm-6">
                        <div class="copyright">
                            <a href="{{url('')}}"> Kim Phúc Long 2016</a>
                        </div>
                    </div>
                    <div class="col-sm-6">
                        <div class="footer-menu">
                            <ul class="footer-nav">
                                <li><a title="About Us" href="{{url('/gioithieu')}}">GIới thiệu</a></li>
                                <li><a title="Terms &amp; Conditions" href="{{url('/tuyendung')}}">Tuyển dụng</a></li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</footer>