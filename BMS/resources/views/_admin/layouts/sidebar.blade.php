<aside class="main-sidebar">
    <!-- sidebar: style can be found in sidebar.less -->
    <section class="sidebar">
        <!-- Sidebar user panel -->
        <div class="user-panel">
            <div class="pull-left image">
                <img src="{{url('image-user/small/'. Auth::user()->thumbnail)}}" class="img-circle" alt="User Image">
            </div>
            <div class="pull-left info">
                <p>
                    @if(Auth::check())
                        {{Auth::user()->name}}
                        @endif
                </p>
                <a href="#"><i class="fa fa-circle text-success"></i> Online</a>
            </div>
        </div>
        <!-- search form -->
        <form action="#" method="get" class="sidebar-form">
            <div class="input-group">
                <input type="text" name="q" class="form-control" placeholder="Search...">
              <span class="input-group-btn">
                <button type="submit" name="search" id="search-btn" class="btn btn-flat"><i class="fa fa-search"></i></button>
              </span>
            </div>
        </form>
        <!-- /.search form -->
        <!-- sidebar menu: : style can be found in sidebar.less -->
        <ul class="sidebar-menu">
            <li class="header">MAIN NAVIGATION</li>

            @foreach($sidebar['menu'] as $key=> $value)
                <li class="treeview {{$sidebar['treeview'] ==  $key ? 'active' : ''}}">
                    <a href="#">
                        <i class="{{$value['icon']}}"></i> <span>{{$value['name']}}</span> <i class="fa fa-angle-left pull-right"></i>
                    </a>
                    <ul class="treeview-menu">
                        <li class="{{$sidebar['action'] == 'create'? 'active': ''}}"><a href="{{url('admin/'.$key .'/create')}}"><i class="fa fa-circle-o"></i> Add new</a></li>
                        <li class="{{$sidebar['action'] == 'index'? 'active': ''}}"><a href="{{url('admin/' . $key)}}"><i class="fa fa-circle-o"></i> List</a></li>
                    </ul>
                </li>
                @endforeach
            <li class="header">SETTINGS</li>
            <li><a href="{{url('admin/setting/' . Auth::user()->id)}}"><i class="fa fa-circle-o text-red"></i> <span>Setting</span></a></li>
            <li><a href="#"><i class="fa fa-circle-o text-yellow"></i> <span>Warning</span></a></li>
            <li><a href="#"><i class="fa fa-circle-o text-aqua"></i> <span>Information</span></a></li>
        </ul>
    </section>
    <!-- /.sidebar -->
</aside>