@extends('_admin.layouts.main')
@section('content')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Video
                <small>it all starts here</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Examples</a></li>
                <li class="active">Blank page</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            @if($request->session()->has('status'))
                <div class="alert alert-success">{{$request->session()->pull('status', '')}}
                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                </div>
        @endif
        <!-- Default box -->
            <div class="row">
                <div class="col-xs-12">
                    <div class="box">
                        <div class="box-header">
                            <h3 class="box-title">Responsive Hover Table</h3>

                            <div class="box-tools">
                                <form action="{{url('admin/post')}}" method="get">

                                    <div style="width: 150px;" class="input-group">
                                        <input type="text" placeholder="Search"
                                               class="form-control input-sm pull-right" name="keyword">
                                        <div class="input-group-btn">
                                            <button class="btn btn-sm btn-default"><i class="fa fa-search"></i>
                                            </button>
                                        </div>
                                    </div>
                                </form>

                            </div>
                        </div><!-- /.box-header -->
                        <div class="box-body no-padding">
                            <table class="table table-hover">

                                <div class="container">
                                </div>
                                <tbody>
                                <tr>
                                    <th>ID</th>
                                    <th>Title</th>
                                    <th>Excerpt</th>
                                    <th>Status</th>
                                    <th>Option</th>
                                </tr>

                                @foreach ($videos as $post)
                                    <tr>
                                        <td>{{$post->id}}</td>
                                        <td>{{$post->title}}</td>
                                        <td>{{$post->updated_at}}</td>
                                        <td>
                                            @if($post->id != 39)
                                                <span class="label {{$post->status != 'draft' ? 'label-success' : 'label-default'}}">{{$post->status}}</span>
                                                @else
                                                <span class="label label-warning">Video Trang chủ</span>
                                            @endif

                                        </td>
                                        <td width="400">{{$post->excerpt}}</td>
                                        <td>
                                            <div class="btn-group">
                                                <button class="btn btn-success btn-sm" type="button">Action</button>
                                                <button data-toggle="dropdown"
                                                        class="btn btn-success btn-sm dropdown-toggle" type="button"
                                                        aria-expanded="false">
                                                    <span class="caret"></span>
                                                    <span class="sr-only">Toggle Dropdown</span>
                                                </button>
                                                <ul role="menu" class="dropdown-menu">
                                                    <li><a href="{{url('admin/video/edit/'. $post->id)}}">Edit</a>
                                                    </li>
                                                    @if($post->id != 39)
                                                        <li>
                                                            <a href="{{url('admin/video/delete/' . $post->id)}}">Delete</a>
                                                        </li>
                                                    @endif

                                                </ul>
                                            </div>
                                        </td>
                                    </tr>
                                @endforeach

                                </tbody>
                            </table>
                        </div><!-- /.box-body -->
                        <div class="box-footer clearfix ">
                            <div class="pull-right">
                                {{$videos->appends(['keyword' => $request->keyword])->links()}}
                            </div>
                        </div>
                    </div><!-- /.box -->
                </div>
            </div>

        </section><!-- /.content -->
    </div>

@stop