@extends('_admin.layouts.main')

@section('content')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Video page
                <small>it all starts here</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Examples</a></li>
                <li class="active">Blank page</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">New Video</h3>
                    <div class="box-tools pull-right">
                        {{--<button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>--}}
                        {{--<button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>--}}
                    </div>
                </div>
                <div class="box-body">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form role="form" method="post" action="" enctype="multipart/form-data">
                        {!! csrf_field() !!}
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-9">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Title</label>
                                        <input name="title" value="{{old('title')}}" type="text"
                                               placeholder="Enter title" id="" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Product</label>
                                        {{Form::select('productid', $products, '', ['class' => 'form-control'])}}

                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Short Description</label>
                                        <input name="excerpt" value="{{old('excerpt')}}" type="text"
                                               placeholder="Excerpt" id="" class="form-control">
                                    </div>

                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Url youtube</label>
                                        <input name="link" value="{{old('link')}}" type="text"
                                               placeholder="link" id="" class="form-control">
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label for="exampleInputEmail1">Status</label>
                                        {{Form::select('status', $status,'publish', ['class' => 'form-control'])}}
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputFile">Thumbnail</label>
                                        <input name="image" type="file" id="">
                                        <p class="help-block">Example block-level help text here.</p>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.box-body -->
                        <div class="box-footer">
                            <button class="btn btn-primary" type="submit">Submit</button>
                        </div>
                    </form>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    Footer
                </div><!-- /.box-footer-->
            </div><!-- /.box -->

        </section><!-- /.content -->
    </div>
@stop

@section('script')
    <script src="https://cdn.ckeditor.com/4.4.3/full-all/ckeditor.js"></script>
    <script type="text/javascript"
            src="{{URL::asset('public/Assets/plugins/input-mask/jquery.inputmask.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('public/Assets/plugins/ckfinder/ckfinder.js')}}"></script>
    <script>
        $('#price').inputmask('999 999 999', {
            placeholder: ''
        });
        $('.number').inputmask('999', {
            placeholder: ''
        });

    </script>

@endsection