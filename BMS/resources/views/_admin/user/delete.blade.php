@extends('_admin.layouts.main')

@section('content')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
               Delete user
                <small>it all starts here</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Examples</a></li>
                <li class="active">Blank page</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title text-red">Do you want to delete this user?</h3>
                    <div class="box-tools pull-right">
                        {{--<button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>--}}
                        {{--<button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>--}}
                    </div>
                </div>
                <div class="box-body">
                    <div class="row">
                        <div class="col-md-9">
                            <div class="row">
                            	<div class="col-md-12"><h4>{{$user->name}}</h4></div>
                                <div class="col-md-12">
                                	<dl>
                                	<dt>Email</dt>
                                	<dd>{{$user->email}}</dd>
                                	<dt>Date create</dt>
                                	<dd>{{date('m/d/Y',strtotime($user->datecreate))}}</dd>
                                        <dt>Date modified</dt>
                                        <dd>{{date('m/d/Y', strtotime($user->datemodified))}}</dd>
                                </dl>
                                </div>
                            </div>
                            
                        </div>
                        <div class="col-md-3">
                            <img src="{{url('public/media/userimage/'. $user->thumbnail)}}"/>
                        </div>
                    </div><!--
                        </div>
                        <div clas="col-md-3"></div>
                    </div>
                 /.box-body -->
                    <form method="post">
                        {{csrf_field()}}
                        <input type="hidden" name="id" value="{{$user->id}}" />
                        <button  class="btn btn-danger">Xóa</button>
                    </form>
                    <br/>
                    <div class="box-footer">
                        <a href="{{url('admin/user/edit/'. $user->id)}}" class="btn btn-success">Sửa</a>
                    </div>
                </div><!-- /.box -->

        </section><!-- /.content -->
    </div>
@stop

@section('script')
    <script src="https://cdn.ckeditor.com/4.4.3/full-all/ckeditor.js"></script>
    <script>
        CKEDITOR.replace('editor1');
    </script>

@endsection