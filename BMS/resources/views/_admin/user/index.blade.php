@extends('_admin.layouts.main')

@section('content')

	<div class="content-wrapper">
		<!-- Content Header (Page header) -->
		<section class="content-header">
			<h1>
				Post page
				<small>it all starts here</small>
			</h1>
			<ol class="breadcrumb">
				<li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
				<li><a href="#">Examples</a></li>
				<li class="active">Blank page</li>
			</ol>
		</section>

		<!-- Main content -->
		<section class="content">

			<!-- Default box -->
			<div class="row">
				<div class="col-xs-12">
					<div class="box">
						<div class="box-header">
							<h3 class="box-title">Responsive Hover Table</h3>
							<div class="box-tools">
								<form method="get">
									<div style="width: 150px;" class="input-group">
										<input type="text" placeholder="Search" class="form-control input-sm pull-right" name="keyword">
										<div class="input-group-btn">
											<button class="btn btn-sm btn-default"><i class="fa fa-search"></i></button>
										</div>
									</div>
								</form>
							</div>
						</div><!-- /.box-header -->
						<div class="box-body no-padding">
							<table class="table table-hover">

								<div class="container">
								</div>
								<tbody><tr>
									<th>ID</th>
									<th>Name</th>
									<th>email</th>
									<th>Status</th>
									<th width="20%">phone</th>
									<th>Type</th>
								</tr>

								@foreach ($users as $user)
									<tr>
										<td>{{$user->id}}</td>
										<td>{{$user->name}}</td>
										<td>{{$user->email}}</td>
										<td><span class="label label-{{$user->status == 'deactive' ? 'danger': 'success'}}">{{$user->status}}</span></td>
										<td width="400">{{$user->phone}}</td>
										<td><span class="label label-{{$user->role== 'user' ? 'warning': 'primary'}}">{{$user->role}}</span></td>
										<td>
											<div class="btn-group">
												<button class="btn btn-success btn-sm" type="button">Action</button>
												<button data-toggle="dropdown" class="btn btn-success btn-sm dropdown-toggle" type="button" aria-expanded="false">
													<span class="caret"></span>
													<span class="sr-only">Toggle Dropdown</span>
												</button>
												<ul role="menu" class="dropdown-menu">
													<li><a href="{{url('admin/user/show/'. $user->id)}}">View</a></li>
													<li><a href="{{url('admin/user/edit/'. $user->id)}}">Edit</a></li>
													<li><a href="{{url('admin/user/delete/' . $user->id)}}">Delete</a></li>

												</ul>
											</div>
										</td>
									</tr>
								@endforeach

								</tbody>
							</table>
						</div><!-- /.box-body -->
						<div class="box-footer clearfix ">
							<div class="pull-right">
								{!! $users->appends(['keyword' => $request->keyword])->links() !!}

							</div>
						</div>
					</div><!-- /.box -->
				</div>
			</div>

		</section><!-- /.content -->
	</div>

@stop