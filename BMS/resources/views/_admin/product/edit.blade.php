@extends('_admin.layouts.main')

@section('content')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Product page
                <small>it all starts here</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Examples</a></li>
                <li class="active">Blank page</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">

            <!-- Default box -->
            <div class="box">
                <div class="box-header with-border">
                    <h3 class="box-title">New Product</h3>
                    <div class="box-tools pull-right">
                        {{--<button class="btn btn-box-tool" data-widget="collapse" data-toggle="tooltip" title="Collapse"><i class="fa fa-minus"></i></button>--}}
                        {{--<button class="btn btn-box-tool" data-widget="remove" data-toggle="tooltip" title="Remove"><i class="fa fa-times"></i></button>--}}
                    </div>
                </div>
                <div class="box-body">
                    @if (count($errors) > 0)
                        <div class="alert alert-danger">
                            <ul>
                                @foreach ($errors->all() as $error)
                                    <li>{{ $error }}</li>
                                @endforeach
                            </ul>
                        </div>
                    @endif
                    <form role="form" method="post" action="" enctype="multipart/form-data">
                        {!! csrf_field() !!}
                        <div class="box-body">
                            <div class="row">
                                <div class="col-md-9">

                                    <div class="form-group">
                                        <label >Title</label>
                                        <input name="title" value="{{$post->title}}" type="text"
                                               placeholder="Enter title" id="" class="form-control">
                                    </div>
                                    <div class="form-group">
                                        <label for="exampleInputPassword1">Category</label>
                                        {{Form::select('categoryid', $categories, $post->category_id, ['class' => 'form-control'])}}

                                    </div>

                                    <div class="form-group hidden">
                                        <label >Quantity</label>
                                        <input type="number" value="{{$post->quantity}}" name="quantity" class="form-control"/>
                                    </div>
                                    <div class="form-group">
                                        <label>Short Description</label>
                                        <input name="excerpt" value="{{$post->excerpt}}" type="text"
                                               placeholder="Excerpt" id="" class="form-control">
                                    </div>

                                    <div class="form-group">
                                        <label >Description</label>
                                        <textarea name="description" id="editor1">{{$post->description}}</textarea>
                                    </div>
                                </div>
                                <div class="col-md-3">
                                    <div class="form-group">
                                        <label >Status</label>
                                        {{Form::select('status', $status, $post->status, ['class' => 'form-control'])}}
                                    </div>
                                    <div class="form-group">
                                        <label >Thumbnail</label>
                                        <input name="image" type="file" id="">
                                        <img src="{{URL::asset('public/media/images/'. $post->thumbnail)}}"  width="100%"  alt="" class="src">
                                        <p class="help-block">Example block-level help text here.</p>
                                    </div>
                                </div>
                            </div>
                        </div><!-- /.box-body -->
                        <div class="box-footer">
                            <button class="btn btn-primary" type="submit">Submit</button>
                        </div>
                    </form>
                </div><!-- /.box-body -->
                <div class="box-footer">
                    Footer
                </div><!-- /.box-footer-->
            </div><!-- /.box -->

        </section><!-- /.content -->
    </div>
@stop

@section('script')
    <script src="https://cdn.ckeditor.com/4.4.3/full-all/ckeditor.js"></script>
    <script type="text/javascript"
            src="{{URL::asset('public/assets/plugins/input-mask/jquery.inputmask.js')}}"></script>
    <script type="text/javascript" src="{{URL::asset('public/assets/plugins/ckfinder/ckfinder.js')}}"></script>
    <script>
        $('#price').inputmask('999 999 999', {
            placeholder: ''
        });
        $('.number').inputmask('999', {
            placeholder: ''
        });
        var editor =   CKEDITOR.replace('editor1', {
            height: '500px'
        });
        CKFinder.setupCKEditor(editor, '{{URL::asset('public/assets/plugins/ckfinder/')}}');
        $(function () {
            $('#sale').click(function () {
                if ($(this).is(':checked')) {
                    $('body .box-sale').show();
                } else {
                    $('body .box-sale').hide();
                }
            })
        })
    </script>

@endsection
