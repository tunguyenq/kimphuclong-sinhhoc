@extends('_admin.layouts.main')

@section('content')

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                User page
                <small>it all starts here</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Examples</a></li>
                <li class="active">Blank page</li>
            </ol>
        </section>
        <!-- Main content -->
        <section class="content">
            <!-- Default box -->
            <form class="form-horizontal"  method="post" enctype="multipart/form-data" >
            <div class="row">
                <div class="col-md-3">
                    <div class="box box-primary">
                        <div class="box-body box-profile">
                            <img alt="User profile picture" src="{{url('image-user/small/' . Auth::user()->thumbnail )}}" class="profile-user-img img-responsive img-circle">
                            <h3 class="profile-username text-center">{{Auth::user()->name}}</h3>
                            <p class="text-muted text-center">Công Ty</p>


                        </div>
                    </div>
                </div>
                <div class="col-md-9">
                    <div class="box box-primary">
                        <div class="box-body">
                                {{csrf_field()}}
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="inputName">Name</label>
                                    <div class="col-sm-10">
                                        <input  name="name" value="{{Auth::user()->name}}" placeholder="Name"  class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="inputEmail">Email</label>
                                    <div class="col-sm-10">
                                        <input name="email" type="email" placeholder="Email" value="{{Auth::user()->email}}"  class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-sm-2 control-label" for="inputName">Phone</label>
                                    <div class="col-sm-10">
                                        <input name="phone" type="text" value="{{Auth::user()->phone}}" placeholder="Phone"  class="form-control">
                                    </div>
                                </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="inputName">Avatar</label>
                                <div class="col-sm-10">
                                    <input type="file" name="image" />
                                </div>
                            </div>
                                <div class="form-group">
                                    <div class="col-sm-offset-2 col-sm-10">
                                        <button class="btn btn-danger" type="submit">Save</button>
                                    </div>
                                </div>
                        </div>
                    </div>





                    <div class="box box-primary">
                        <div class="box-header">
                            Đổi mật khẩu
                        </div>
                        <div class="box-body">
                            {{csrf_field()}}
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="inputName">Mật khẩu cũ</label>
                                <div class="col-sm-10">
                                    <input  name="name" value="" placeholder="Mật khẩu cũ"  class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="inputEmail">Mật khẩu mới</label>
                                <div class="col-sm-10">
                                    <input name="email" type="password" placeholder="Mật khẩu mới" value="Mật khẩu mới"  class="form-control">
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="inputEmail">Nhập lại mật khẩu mới</label>
                                <div class="col-sm-10">
                                    <input name="email" type="password" placeholder="NHập lại mật khẩu" value=""  class="form-control">
                                </div>
                            </div>

                            <div class="form-group">
                                <div class="col-sm-offset-2 col-sm-10">
                                    <button class="btn btn-success" type="submit">Save</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>





            </div>
            </form>
        </section><!-- /.content -->
    </div>
@stop

@section('script')

@endsection