@extends('layouts.app')
<style>
    .sitesao-preview {
        display: none;
    }

    .wp__version {
        display: none;
    }

    .pagination {
        display: inline-block;
        padding-left: 0;
        margin: 20px 0;
        border-radius: 4px;
    }

    .pagination > li {
        display: inline;
    }

    .pagination > li > a,
    .pagination > li > span {
        position: relative;
        float: left;
        padding: 6px 12px;
        margin-left: -1px;
        line-height: 1.42857143;
        color: #337ab7;
        text-decoration: none;
        background-color: #fff;
        border: 1px solid #ddd;
    }

    .pagination > li:first-child > a,
    .pagination > li:first-child > span {
        margin-left: 0;
        border-top-left-radius: 4px;
        border-bottom-left-radius: 4px;
    }

    .pagination > li:last-child > a,
    .pagination > li:last-child > span {
        border-top-right-radius: 4px;
        border-bottom-right-radius: 4px;
    }

    .pagination > li > a:hover,
    .pagination > li > span:hover,
    .pagination > li > a:focus,
    .pagination > li > span:focus {
        z-index: 3;
        color: #23527c;
        background-color: #eee;
        border-color: #ddd;
    }

    .pagination > .active > a,
    .pagination > .active > span,
    .pagination > .active > a:hover,
    .pagination > .active > span:hover,
    .pagination > .active > a:focus,
    .pagination > .active > span:focus {
        z-index: 2;
        color: #fff;
        cursor: default;
        background-color: #337ab7;
        border-color: #337ab7;
    }

    .pagination > .disabled > span,
    .pagination > .disabled > span:hover,
    .pagination > .disabled > span:focus,
    .pagination > .disabled > a,
    .pagination > .disabled > a:hover,
    .pagination > .disabled > a:focus {
        color: #777;
        cursor: not-allowed;
        background-color: #fff;
        border-color: #ddd;
    }

    .pagination-lg > li > a,
    .pagination-lg > li > span {
        padding: 10px 16px;
        font-size: 18px;
        line-height: 1.3333333;
    }

    .pagination-lg > li:first-child > a,
    .pagination-lg > li:first-child > span {
        border-top-left-radius: 6px;
        border-bottom-left-radius: 6px;
    }

    .pagination-lg > li:last-child > a,
    .pagination-lg > li:last-child > span {
        border-top-right-radius: 6px;
        border-bottom-right-radius: 6px;
    }

    .pagination-sm > li > a,
    .pagination-sm > li > span {
        padding: 5px 10px;
        font-size: 12px;
        line-height: 1.5;
    }

    .pagination-sm > li:first-child > a,
    .pagination-sm > li:first-child > span {
        border-top-left-radius: 3px;
        border-bottom-left-radius: 3px;
    }

    .pagination-sm > li:last-child > a,
    .pagination-sm > li:last-child > span {
        border-top-right-radius: 3px;
        border-bottom-right-radius: 3px;
    }

    .pager {
        padding-left: 0;
        margin: 20px 0;
        text-align: center;
        list-style: none;
    }

    .pager li {
        display: inline;
    }

    .pager li > a,
    .pager li > span {
        display: inline-block;
        padding: 5px 14px;
        background-color: #fff;
        border: 1px solid #ddd;
        border-radius: 15px;
    }

    .pager li > a:hover,
    .pager li > a:focus {
        text-decoration: none;
        background-color: #eee;
    }

    .pager .next > a,
    .pager .next > span {
        float: right;
    }

    .pager .previous > a,
    .pager .previous > span {
        float: left;
    }

    .pager .disabled > a,
    .pager .disabled > a:hover,
    .pager .disabled > a:focus,
    .pager .disabled > span {
        color: #777;
        cursor: not-allowed;
        background-color: #fff;
    }
</style>
@section('content')
    <div class="content-container">
        <div class="container">


            <div class="row">
                <div class="column col-md-9">

                    <div class="row">
                        <div class="col-sm-12">
                            <div class="revslider">
                                <div id="rev_slider_2_1_wrapper" class="rev_slider_wrapper fullwidthbanner-container">
                                    <div id="rev_slider_2_1" class="rev_slider fullwidthabanner">
                                        <ul>
                                            @foreach($sliders as $slider)
                                                <li data-transition="random" data-slotamount="7" data-masterspeed="300"
                                                    data-saveperformance="off">
                                                    <img src='{{URL::asset('public/media/images/' . $slider->thumbnail)}}'
                                                         alt="home-slide" data-bgposition="center top"

                                                         data-bgfit="cover" data-bgrepeat="no-repeat">

                                                    <div class="tp-caption theairslicesubtitle sft tp-resizeme"
                                                         data-x="195"
                                                         data-y="291"
                                                         data-speed="300" data-start="500"
                                                         data-easing="Power3.easeInOut"
                                                         data-splitin="none"
                                                         data-splitout="none" data-elementdelay="0.1"
                                                         data-endelementdelay="0.1"
                                                         data-endspeed="300">
                                                        <span>{{$slider->excerpt}}</span>

                                                    </div>
                                                    <div class="tp-caption sft tp-resizeme" data-x="481" data-y="467"
                                                         data-speed="300"
                                                         data-start="1400" data-easing="Power3.easeInOut"
                                                         data-splitin="none"
                                                         data-splitout="none" data-elementdelay="0.1"
                                                         data-endelementdelay="0.1"
                                                         data-endspeed="300">
                                                        <button class="btn btn-custom-color btn-block btn-default btn-style-square btn-effect-bg-fade-in"
                                                                type="button" data-hover-color="#333"
                                                                data-hover-border-color="#ffffff"
                                                                data-hover-background-color="#ffffff">
                                                            <span>{{$slider->title}}</span>
                                                        </button>
                                                    </div>
                                                </li>
                                            @endforeach
                                        </ul>
                                        <div class="tp-bannertimer tp-bottom"></div>
                                    </div>
                                </div>

                            </div>
                        </div>

                    </div>


                    <div class="latestnews latestnews-only">
                        <div class="latestnews-title">
                            <h3 class="el-heading">
                                <a href="#"><i class="fa fa-chevron-right"></i>Sản phẩm</a>
                            </h3>
                        </div>
                    </div>
                    <div class="posts infinite-scroll masonry" data-paginate="infinite_scroll" data-layout="masonry"
                         data-masonry-column="3">
                        <div class="posts-wrap infinite-scroll-wrap masonry-wrap posts-layout-masonry row">

                            @foreach($productmains as $product)
                                <article class="infinite-scroll-item masonry-item col-md-4 col-sm-6 hentry">
                                    <div class="hentry-wrap">
                                        <div class="entry-featured image-featured">
                                            <a href="{{url('sanpham/chi-tiet/' . $product->slug . '/' . $product->id)}}">
                                                <img width="500" height="500"
                                                     src='{{URL::asset('public/media/images/' . $product->thumbnail)}}'
                                                     alt=""/>
                                            </a>
                                        </div>
                                        <div class="entry-info">
                                            <div class="entry-header">
                                                <div class="date-badge">
                                                    <span class="date">28</span>
                                                    <span class="month-year">Oct 2014</span>
                                                </div>
                                                <h2 class="entry-title">
                                                    <a href="{{url('sanpham/chi-tiet/' . $product->slug . '/' . $product->id)}}"
                                                       title="Nullam ullamcorper">
                                                        {{$product->title}}
                                                    </a>
                                                </h2>

                                            </div>
                                            <div class="entry-content">
                                                <p>
                                                    {{$product->excerpt}}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                            @endforeach


                        </div>
                    </div>


                    <div class="latestnews latestnews-only">
                        <div class="latestnews-title">
                            <h3 class="el-heading">
                                <a href="#"><i class="fa fa-chevron-right"></i>Video</a>
                            </h3>
                        </div>
                    </div>
                    <div class="posts infinite-scroll masonry" data-paginate="infinite_scroll" data-layout="masonry"
                         data-masonry-column="3">
                        <div class="posts-wrap infinite-scroll-wrap masonry-wrap posts-layout-masonry row">

                            @foreach($videos as $product)
                                <article class="infinite-scroll-item masonry-item col-md-4 col-sm-6 hentry">
                                    <div class="hentry-wrap">
                                        <div class="entry-featured image-featured">
                                            <a href="{{url('video/chi-tiet/' . $product->slug . '/' . $product->id)}}" class="link-video" data-link="{{$product->description}}">
                                                <img width="500" height="500"
                                                     src='{{URL::asset('http://img.youtube.com/vi/'. $product->description .'/0.jpg')}}' alt=""/>
                                            </a>
                                        </div>
                                        <div class="entry-info">
                                            <div class="entry-header">
                                                <div class="date-badge">
                                                    <span class="date">28</span>
                                                    <span class="month-year">Oct 2014</span>
                                                </div>
                                                <h2 class="entry-title">
                                                    <a href="{{url('video/chi-tiet/' . $product->slug . '/' . $product->id)}}"
                                                       title="Nullam ullamcorper">
                                                        {{$product->title}}
                                                    </a>
                                                </h2>
                                            </div>
                                            <div class="entry-content">
                                                <p>
                                                    {{$product->title}}
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </article>
                            @endforeach


                        </div>
                    </div>



                </div>
                <aside class="col-md-3 sidebar-wrap">
                    @include('layouts.sidebar-right')
                </aside>
            </div>
        </div>
    </div>
    <a href="#" class="go-to-top"><i class="fa fa-angle-up"></i></a>
    <div class="sitesao-preview">
        <div class="sitesao-preview__wrap">
            <a href="#" class="sitesao-preview__control"><i class="fa fa-spin fa-cog"></i></a>
            <div class="sitesao-preview__header">
                <h4>Demo</h4>
            </div>
            <div class="sitesao-preview__content">
                <div class="sitesao-preview__content__section">
                    <div class="sitesao-preview__content__section__layout">
                        <label>Layout Style</label>
                        <select data-name="layout-style">
                            <option selected="selected" value="wide">Wide</option>
                            <option value="boxed">Boxed</option>
                        </select>
                    </div>

                </div>
            </div>
        </div>
    </div>
    <div class="sitesao-preview__loading">
        <div class="sitesao-preview__loading__animation"><i></i><i></i><i></i><i></i></div>
    </div>


@endsection