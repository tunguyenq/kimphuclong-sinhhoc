<?php

namespace Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Post extends Model
{
    //
    public static function getPostsByObjectType($objectType, $keyword, $paginate)
    {
        if($keyword != '') {
            $users = DB::table('posts')
                ->where('object_type', '=', $objectType)
                ->where('title', 'like', "%$keyword%")
                ->select('id', 'title', 'excerpt', 'updated_at', 'created_at')
                ->paginate($paginate);
            return $users;
        }
        else{
        $users = DB::table('posts')
            ->where('object_type', '=', $objectType)
            ->select('id', 'title', 'excerpt', 'updated_at', 'created_at')
            ->paginate($paginate);
        return $users;

    }

    }

    public static function updatePost($args, $id){
        return DB::table('posts')
            ->where('id', '=', $id)
            ->update($args);
    }
    public static function insertGetId($args)
    {
        return DB::table('posts')->insertGetId(
            $args
        );
    }

}
