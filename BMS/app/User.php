<?php

namespace App;

use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
class User extends Authenticatable
{

    protected $primaryKey  = 'id';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'name', 'email', 'password',
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public static function hasRole($role)
    {
        if (Auth::user()->role == $role)
            return true;
        else
            return false;
    }
    public static function getUsers($paginate, $keyword){
        if($keyword == ''){
        return DB::table('users')
            ->select('name', 'id', 'updated_at', 'created_at', 'phone', 'email', 'status', 'role')
            ->paginate($paginate);
            }
        else{
            return DB::table('users')
                ->select('name', 'id', 'updated_at', 'created_at', 'phone', 'email', 'status', 'role')
                ->where('name', 'like', "%$keyword%")
                ->orWhere('name', 'like', "%$keyword%")
            ->paginate($paginate);
        }
    }
}
