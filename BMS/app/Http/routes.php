<?php

/*
|--------------------------------------------------------------------------
| Routes File
|--------------------------------------------------------------------------
|
| Here is where you will register all of the routes in an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

use Intervention\Image\ImageManagerStatic as Image;

Route::get('demo/{size}/{filename}', function ($size, $filename) {
    $wh = Config::get('image.templates.' . $size);
    $img = Image::make(Config::get('image.path') . $filename);
    $img->fit($wh['w'], $wh['h']);
    return $img->response();
});
Route::get('image-user/{size}/{filename}', function ($size, $filename) {
    $wh = Config::get('image.templates.' . $size);
    $img = Image::make(Config::get('image.path-user') . $filename);
    $img->fit($wh['w'], $wh['h']);
    return $img->response();
});


Route::get('/', 'HomeController@index');
Route::get('site/create', 'SiteController@create');
Route::get('site', 'SiteController@index');

Route::get('/sanpham/{slug}/{categoryid}', 'ProductController@index');
Route::get('/sanpham/chi-tiet/{slug}/{id}', 'ProductController@detail');
Route::get('/gioi-thieu', 'HomeController@intro');
Route::get('/dai-ly', 'HomeController@supplier');
Route::get('/tuyen-dung', 'HomeController@recruitment');
Route::get('/khuyen-mai', 'HomeController@free');
Route::get('/lien-he', 'HomeController@contact');
Route::get('/video', 'ProductController@video');
Route::get('/tin-tuc', 'HomeController@news');
Route::get('/tin-tuc/chi-tiet/{slug}/{id}', 'HomeController@detailnews');
Route::get('/video/chi-tiet/{slug}/{id}', 'ProductController@detailvideo');


Route::group(['namespace' => 'Admin', 'prefix' => 'admin', 'middleware' => 'web'], function () {


    Route::get('/', 'AdminController@login');
    Route::post('/', 'AdminController@login');
    Route::get('logout', function () {
        Auth::guard()->logout();
        return redirect('/admin');
    });
    Route::get('profile/{id}', 'AdminController@profile');
    Route::post('profile/{id}', 'AdminController@profile');
    Route::get('setting/{id}', 'AdminController@setting');


    Route::get('post', 'PostController@index');
    Route::get('post/create', 'PostController@store');
    Route::post('post/create', 'PostController@store');
    Route::get('post/edit/{id}', 'PostController@update');
    Route::post('post/edit/{id}', 'PostController@update');
    Route::get('post/show/{id}', 'PostController@show');
    Route::get('post/delete/{id}', 'PostController@destroy');
    Route::post('post/delete/{id}', 'PostController@destroy');


    /** PAGE */
    Route::get('page', 'PageController@index');
    Route::get('page/show/{id}', 'PageController@show');
    Route::get('page/create', 'PageController@store');
    Route::post('page/create', 'PageController@store');
    Route::get('page/edit/{id}', 'PageController@update');
    Route::post('page/edit/{id}', 'PageController@update');
    /** NEWS */
    Route::get('news', 'NewsController@index');
    Route::get('news/show/{id}', 'NewsController@show');
    Route::get('news/edit/{id}', 'NewsController@edit');
    Route::get('news/create', 'NewsController@store');
    Route::post('news/create', 'NewsController@store');
    Route::get('news/edit/{id}', 'NewsController@update');
    Route::post('news/edit/{id}', 'NewsController@update');
    Route::get('news/delete/{id}', 'NewsController@destroy');
    Route::post('news/delete/{id}', 'NewsController@destroy');
    /** USER */
    Route::get('user', 'UserController@index');
    Route::get('user/show/{id}', 'UserController@show');
    Route::get('user/edit/{id}', 'UserController@edit');
    Route::get('user/create', 'UserController@create');
    Route::post('user/create', 'UserController@create');
    Route::get('user/edit/{id}', 'UserController@edit');
    Route::post('user/edit/{id}', 'UserController@edit');
    Route::get('user/delete/{id}', 'UserController@destroy');
    Route::post('user/delete/{id}', 'UserController@destroy');


    /** PRODUCT */
    Route::get('product', 'ProductController@index');
    Route::get('product/create', 'ProductController@store');
    Route::post('product/create', 'ProductController@store');
    Route::get('product/edit/{id}', 'ProductController@update');
    Route::post('product/edit/{id}', 'ProductController@update');
    Route::get('product/delete/{id}', 'ProductController@destroy');
    Route::post('product/delete/{id}', 'ProductController@destroy');


    /** CATEGORY */
    Route::get('category', 'CategoryController@index');
    Route::get('category/create', 'CategoryController@store');
    Route::post('category/create', 'CategoryController@store');
    Route::get('category/edit/{id}', 'CategoryController@update');
    Route::post('category/edit/{id}', 'CategoryController@update');
    Route::get('category/delete/{id}', 'CategoryController@destroy');
    Route::post('category/delete/{id}', 'CategoryController@destroy');
    /** VIDEO */
    Route::get('video', 'VideoController@index');
    Route::get('video/create', 'VideoController@store');
    Route::post('video/create', 'VideoController@store');
    Route::get('video/edit/{id}', 'VideoController@update');
    Route::post('video/edit/{id}', 'VideoController@update');
    Route::get('video/delete/{id}', 'VideoController@destroy');
    Route::post('video/delete/{id}', 'VideoController@destroy');
    /** Slider */
    Route::get('slider', 'SliderController@index');
    Route::get('slider/create', 'SliderController@store');
    Route::post('slider/create', 'SliderController@store');
    Route::get('slider/edit/{id}', 'SliderController@update');
    Route::post('slider/edit/{id}', 'SliderController@update');
    Route::get('slider/delete/{id}', 'SliderController@destroy');
    Route::post('slider/delete/{id}', 'SliderController@destroy');

});

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| This route group applies the "web" middleware group to every route
| it contains. The "web" middleware group is defined in your HTTP
| kernel and includes session state, CSRF protection, and more.
|
*/


Route::group(['middleware' => ['web']], function () {
    //
});

//Route::group(['middleware' => 'web'], function () {
//    Route::auth();
//    Route::get('test' , function(){
//        echo Auth::user()->name;
//    });
//    Route::get('/home', 'HomeController@index');
//});
//
