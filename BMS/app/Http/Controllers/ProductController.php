<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Post;
use Illuminate\Http\Request;

use App\Http\Requests;

class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index($slug, $category = 0)
    {
        $data['sbvideo'] = Post::find(39);
        $data['products'] = Post::where('category_id', $category)->where('object_type', 'product')->paginate(9);
        $data['categories'] = Category::where('object_type', 'category')->get();
        $data['products_sidebar'] = Post::where('object_type', 'product')->take(4)->get();
        return view('product.index', $data);
        //
    }

    public function video()
    {
        $data['sbvideo'] = Post::find(39);
        $data['videos'] = Post::where('object_type', 'video')->paginate(4);
        $data['categories'] = Category::where('object_type', 'category')->get();
        $data['products_sidebar'] = Post::where('object_type', 'product')->take(4)->get();
        return view('product.video', $data);
    }

    public function detailvideo($slug, $id)
    {
        $data['sbvideo'] = Post::find(39);
        $data['video'] = Post::find($id);
        $data['categories'] = Category::where('object_type', 'category')->get();
        $data['products_sidebar'] = Post::where('object_type', 'product')->take(4)->get();
        return view('product.detailvideo', $data);
    }

    public function detail($slug, $id)
    {
        $data['sbvideo'] = Post::find(39);
        $data['categories'] = Category::where('object_type', 'category')->get();
        $data['products_sidebar'] = Post::where('object_type', 'product')->take(4)->get();

        $data['product'] = Post::find($id);
        return view('product.detail', $data);
    }



    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
