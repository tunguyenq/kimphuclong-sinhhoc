<?php

namespace App\Http\Controllers;

use App\Http\Requests;
use App\Models\Category;
use App\Models\Post;
use Illuminate\Http\Request;

class HomeController extends Controller
{


    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $products = Post::where('object_type', 'product')
            ->orderBy('created_at', 'desc')
            ->take(4)->get();
        $data['products'] = $products;
        $data['sbvideo'] = Post::find(39);
        $data['categories'] = Category::where('object_type', 'category')->get();
        $sliders = Post::where('object_type', 'slider')->get();
        $data['sliders'] = $sliders;
        $data['productmains'] = Post::where('object_type', 'product')->paginate(6);// cai nay cho danh sach san pham
        $data['videos'] = Post::where('object_type', 'video')->where('id', '<>', 39)->paginate(6);//
        $data['products_sidebar'] = Post::where('object_type', 'product')->take(4)->get();
        return view('home.index', $data);
    }

    public function intro()
    {
        $data['sbvideo'] = Post::find(39);
        $data['categories'] = Category::where('object_type', 'category')->get();
        $data['products_sidebar'] = Post::where('object_type', 'product')->take(4)->get();
        $data['page'] = Post::find(38);
        return view('home.intro', $data);
    }

    public function supplier()
    {
        $data['sbvideo'] = Post::find(39);
        $data['categories'] = Category::where('object_type', 'category')->get();
        $data['products_sidebar'] = Post::where('object_type', 'product')->take(4)->get();
        $data['page'] = Post::find(44);
        return view('home.intro', $data);
    }

    public function recruitment()
    {
        $data['sbvideo'] = Post::find(39);
        $data['categories'] = Category::where('object_type', 'category')->get();
        $data['products_sidebar'] = Post::where('object_type', 'product')->take(4)->get();
        $data['page'] = Post::find(37);
        return view('home.intro', $data);
    }

    public function free()
    {
        $data['sbvideo'] = Post::find(39);
        $data['categories'] = Category::where('object_type', 'category')->get();
        $data['products_sidebar'] = Post::where('object_type', 'product')->take(4)->get();
        $data['page'] = Post::find(46);
        return view('home.intro', $data);
    }
    public function detailnews($slug, $id)
    {
        $data['news'] = Post::find($id);
        $data['categories'] = Category::where('object_type', 'category')->get();
        $data['products_sidebar'] = Post::where('object_type', 'product')->take(4)->get();
        return view('home.detailnews', $data);
    }


    public function contact()
    {
        $data['sbvideo'] = Post::find(39);
        $data['categories'] = Category::where('object_type', 'category')->get();
        $data['products_sidebar'] = Post::where('object_type', 'product')->take(4)->get();
        $data['page'] = Post::find(36);
        return view('home.intro', $data);
    }
    public function news(){
        $data['sbvideo'] = Post::find(39);
        $data['categories'] = Category::where('object_type', 'category')->get();
        $data['products_sidebar'] = Post::where('object_type', 'product')->take(4)->get();
        $data['news'] = Post::where('object_type', 'news')->paginate(10);
        return view('home.news', $data);
    }
}
