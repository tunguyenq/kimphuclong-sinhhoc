<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Models\Post;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use Mockery\CountValidator\Exception;
use Modules\Admin\data\Menu;
use Validator;

class VideoController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $status = [
        'draft' => 'Draft',
        'publish' => 'Publish'
    ];

    public function index(Request $request)
    {
        $data['sidebar'] = Menu::getMenu('video', 'index');
        $videos = Post::where('object_type', 'video')
            ->paginate(10);
        $data['videos'] = $videos;
        $data['request'] = $request;
        return view('_admin.video.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if ($request->isMethod('post')) {
            return $this->doCreate($request);
        }
        $data['sidebar'] = Menu::getMenu('video', 'create');
        $products = Post::where('object_type', 'product')->lists('title', 'id');
        $data['status'] = $this->status;
        $data['products'] = $products;
        return view('_admin.video.create', $data);
    }


    function doCreate($request)
    {

        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'excerpt' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('admin/video/create')
                ->withErrors($validator)
                ->withInput();
        }
        $input = $request->all();
        $input['thumbnail'] = '';
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            // echo $file->getClientOriginalName();die;
            $img = Image::make($file)->resize(500, 500);
            $img->save('public/media/images/' . $file->getClientOriginalName());
            $input['thumbnail'] = $file->getClientOriginalName();
        }
        $post = new Post();

        $post->title = $request->title;
        $post->object_type = 'video';
        $post->thumbnail = $input['thumbnail'];
        $post->parent_id = $request->productid;
        $post->slug = str_slug($request->title, '-');
        $post->excerpt = $request->excerpt;
        $post->description = $request->link;
        $post->save();


        $request->session()->flash('status', 'Add new video  sucess!' . $request->title);
        return redirect('admin/video/edit/' . $post->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->isMethod('post')) {
            return $this->do_edit($request, $id);
        }

        $data['sidebar'] = Menu::getMenu('video', 'edit');
        $post = Post::find($id);
        $data['request'] = $request;
        $data['post'] = $post;
        $products = Post::where('object_type', 'product')->lists('title', 'id');
        $data['status'] = $this->status;
        $data['products'] = $products;
        return view('_admin.video.edit', $data);
    }

    function do_edit($request, $id)
    {

        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return redirect('admin/video/edit')
                ->withErrors($validator)
                ->withInput();
        }
        $post = Post::find($id);
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            // echo $file->getClientOriginalName();die;
            $img = Image::make($file)->resize(500, 500);
            $img->save('public/media/images/' . $file->getClientOriginalName());
            $post->thumbnail = $file->getClientOriginalName();

        }
        try{
            if ($request->link != '') {
                $pathUrl = parse_url($request->link);
                parse_str($pathUrl['query'], $query);
                $post->description = $query['v'];
            }
        }catch(Exception $ex){

        }
        $post->status = $request->status;
        $post->title = $request->title;
        $post->object_type = 'video';
        $post->parent_id = $request->productid;
        $post->slug = str_slug($request->title, '-');
        $post->excerpt = $request->excerpt;
        $post->save();


        $request->session()->flash('status', 'edit video success!' . $request->title);
        return redirect('admin/video/edit/' . $post->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $post = Post::find($id);
        $data['sidebar'] = Menu::getMenu('video', '');
        $data['post'] = $post;
        if ($request->isMethod('post')) {
            $post = Post::find($id);
            Post::destroy($id);
            return redirect('admin/video')->with('status', 'delete ' . $post->title . ' successful');
        }
        return view('_admin.video.delete', $data);
    }
}
