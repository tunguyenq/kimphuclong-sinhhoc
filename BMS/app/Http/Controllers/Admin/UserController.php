<?php namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\User;
use Illuminate\Support\Facades\Validator;
use Modules\Admin\data\DataValidate;
use App\Http\Controllers\Controller;
use Modules\Admin\data\Menu;
use Modules\Admin\data\Data;

class UserController extends Controller
{

    public function index(Request $request)
    {
        $keyword  = '';
        if($request->has('keyword')){
            $keyword = $request->keyword;
        }
        $users = User::getUsers(10, $keyword);
        $data['users'] = $users;
        $data['sidebar'] = Menu::getMenu('user', 'index');
        $data['request'] = $request;
        return view('_admin.user.index', $data);

    }

    public function show($id)
    {
        $user = User::find($id);
        $data['user'] = $user;
        $data['sidebar'] = Menu::getMenu('user', '');
        return view('_admin/user.show', $data);
    }

    public function edit($id, Request $request)
    {
        $user = User::find($id);
        $data['request'] = $request;
        $data['user'] = $user;
        $data['sidebar'] = Menu::getMenu('user', '');
        $data['roles'] = Data::get('postobject.user.roles');
        $data['status'] = Data::get('postobject.user.status');
        if ($request->isMethod('post')) {
           return  $this->do_edit($request, $id);
        }
        return view('_admin.user.edit', $data);
    }

    function do_edit($request, $id)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email',
        ],
            DataValidate::getMessageError('user'));
        if($validator->fails()){
            return redirect('admin/user/edit/'. $id)
                ->withErrors($validator)
                ->withInput();
        }
        else{
            $input = $request->all();
            $user = User::find($id);
            $user->name = $input['name'];
            $user->phone = $input['phone'];
            $user->email = $input['email'];
            $user->role = $input['role'];
            $user->status = $input['status'];
            $user->save();

            return redirect('admin/user/edit/'.  $user->id)->with('status', 'Edit user successful!');
        }

    }

    public function create(Request $request)
    {
        if ($request->isMethod('post')) {
            return $this->do_create($request);
        }
        $data['sidebar'] = Menu::getMenu('user', 'create');
        $data['roles'] = Data::get('postobject.user.roles');
        return view('_admin.user.create', $data);
    }

    function do_create($request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required',
            'email' => 'required|email|unique:users,email',
            'password' => 'required|confirmed'
        ],
            DataValidate::getMessageError('user'));
        if ($validator->fails()) {
            return redirect('admin/user/create')
                ->withErrors($validator)
                ->withInput();
        } else {
            $input = $request->all();
            $user = new User();
            $user->email = $input['email'];
            $user->name = $input['name'];
            $user->password = bcrypt($input['password']);
            $user->role = $input['role'];
            $user->phone = $input['phone'];
            $user->save();
            return redirect('admin/user/edit/' . $user->id);
        }
    }

    public function destroy($id, Request $request)
    {
        $post = User::find($id);
        $data['sidebar'] = Menu::getMenu('user', '');
        $data['user'] = $post;
        if ($request->isMethod('post')) {
            $user = User::find($id);
            User::destroy($id);
            return redirect('admin/user')->with('status', 'delete ' . $user->name . ' successful');
        }
        return view('_admin.user.delete', $data);
    }

}