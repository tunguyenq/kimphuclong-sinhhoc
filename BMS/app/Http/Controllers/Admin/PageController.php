<?php

namespace App\Http\Controllers\Admin;

use App\Models\Post;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use Modules\Admin\data\Menu;
use Validator;
class PageController extends Controller
{

    public $objectType = 'page';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected function formatErrors(Validator $validator)
    {
        return $validator->errors()->all();
    }

    public function index(Request $request)
    {
        $keyword  = '';
        if($request->has('keyword')){
            $keyword = $request->keyword;
        }
        $data['sidebar'] = Menu::getMenu('page', 'index');
        $customers = Post::where('object_type', $this->objectType)->paginate(10);
        $customers->setPath('page');
        $data['posts'] = $customers;
        $data['request'] = $request;
        return view('_admin.page.index', $data);
    }



    function doCreate($request)
    {

        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'excerpt' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('admin/page/create')
                ->withErrors($validator)
                ->withInput();
        }
        $input['thumbnail'] = '';
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            // echo $file->getClientOriginalName();die;
            $img = Image::make($file)->resize(200, 200);
            $img->save('public/media/images/' . $file->getClientOriginalName());
            $input['thumbnail'] = $file->getClientOriginalName();
        }
        $post = new Post();
        $post->title = $request->title;
        $post->object_type = $this->objectType;
        $post->thumbnail = $input['thumbnail'];
        $post->slug = str_slug($request->title, '-');
        $post->excerpt = $request->excerpt;
        $post->created_at = date('Y-m-d');

        $post->description  = $request->description;
        $post->save();

        $request->session()->flash('status', 'Add new Post success!' . $post->title);
        return redirect('admin/page/edit/' . $post->id);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data['sidebar'] = Menu::getMenu('post', 'create');

        if ($request->isMethod('post')) {
            return $this->doCreate($request);
        }
        return view('_admin.page.create', $data);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $data['sidebar'] = Menu::getMenu('post', '');
        $post = POST::find($id);
        $data['post'] = $post;
        return view('_admin::post.show', $data);
    }

    function do_edit($request, $id)
    {
        //validate data
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'excerpt' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect('admin/page/edit/' . $id)
                ->withErrors($validator)
                ->withInput();
        } else {
            $page = Post::find($id);
            if ($request->hasFile('image')) {
                $file = $request->file('image');
                // echo $file->getClientOriginalName();die;
                Image::make($file)->save('public/media/images/' . $file->getClientOriginalName());
                $page->thumbnail = $file->getClientOriginalName();
            }
            $page->title = $request->title;

            $page->slug = str_slug($request->title, '-');
            $page->created_at = date('Y-m-d');
            $page->description  = $request->description;
//            $page->excerpt = $request->excerpt;
            $page->save();
            $request->session()->flash('status', 'Edit page successful ! ' . $request->title);
            return redirect('admin/page/edit/' . $id);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data['sidebar'] = Menu::getMenu('page', 'edit');
        $post = Post::find($id);
        $data['request'] = $request;
        if ($request->isMethod('post')) {
            return $this->do_edit($request, $id);
        }
        $data['post'] = $post;
        return view('_admin.page.edit', $data);

    }


    public function destroy(Request $request, $id)
    {
        $post = POST::find($id);
        $data['sidebar'] = Menu::getMenu('post', '');
        $data['post'] = $post;
        if($request->isMethod('post')){
            $post = Post::find($id);
            Post::destroy($id);
            return redirect('admin/post')->with('status', 'delete ' . $post->title . ' successful');
        }
        return view('admin::post.delete', $data);
    }

    public function __construct()
    {
        //$this->middleware('auth');
    }
}
