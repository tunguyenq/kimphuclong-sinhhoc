<?php namespace App\Http\Controllers\Admin;

use Intervention\Image\ImageManagerStatic as Image;
use App\Http\Controllers\Controller;
use Models\Post;
use Validator;
use Illuminate\Http\Request;
use App\Http\Controllers;
use Modules\Admin\data\DataValidate;
use Modules\Admin\data\Menu;
use Modules\Admin\data\Data;
class PostController extends Controller
{

    public $objectType = 'post';

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected function formatErrors(Validator $validator)
    {
        return $validator->errors()->all();
    }

    public function index(Request $request)
    {
        $keyword  = '';
        if($request->has('keyword')){
            $keyword = $request->keyword;
        }
        $data['sidebar'] = Menu::getMenu('post', 'index');
        $customers = Post::getPostsByObjectType($this->objectType, $keyword,  2);
        $customers->setPath('post');
        $data['posts'] = $customers;
        $data['request'] = $request;
        return view('_admin.post.index', $data);
    }



    function doCreate($request)
    {

        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'excerpt' => 'required',
        ], DataValidate::getMessageError($this->objectType));

        if ($validator->fails()) {
            return redirect('admin/post/create')
                ->withErrors($validator)
                ->withInput();
        }
        $input = $request->all();
        $input['thumbnail'] = '';
        if ($request->hasFile('image')) {
            $file = $request->file('image');
           // echo $file->getClientOriginalName();die;
            $img = Image::make($file)->resize(200, 200);
                $img->save('public/media/images/' . $file->getClientOriginalName());
            $input['thumbnail'] = $file->getClientOriginalName();
        }
        unset($input['_token']);
        unset($input['image']);
        $input['slug'] = str_slug($input['title'], '-');
        $input['object_type'] = $this->objectType;
        $input['created_at'] = date('Y-m-d');
        //$id = Post::insertGetId($input);
        $post = new Post();
        $post->title = $input['title'];
        $post->object_type = $this->objectType;
        $post->thumbnail = $input['thumbnail'];
        $post->slug = str_slug($input['title'], '-');
        $post->excerpt = $input['excerpt'];
        $post->description  = $input['description'];
        $post->save();


        $request->session()->flash('status', 'Add new Post sucess!' . $post->id);
        return redirect('admin/post/edit/' . $post->id);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data['sidebar'] = Menu::getMenu('post', 'create');

        if ($request->isMethod('post')) {
            return $this->doCreate($request);
        }
        return view('admin::post.create', $data);

    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
        $data['sidebar'] = Menu::getMenu('post', '');
        $post = POST::find($id);
        $data['post'] = $post;
        return view('admin::post.show', $data);
    }

    function do_edit($request, $id)
    {

        //validate data
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'excerpt' => 'required'
        ], DataValidate::getMessageError($this->objectType));


        if ($validator->fails()) {
            return redirect('admin/post/edit/' . $id)
                ->withErrors($validator)
                ->withInput();
        } else {

            $input = $request->all();
            $input['thumbnail'] = '';
            if ($request->hasFile('image')) {
                $file = $request->file('image');
                // echo $file->getClientOriginalName();die;
                Image::make($file)->save('public/media/images/' . $file->getClientOriginalName());
                $input['thumbnail'] = $file->getClientOriginalName();
            }
            unset($input['_token']);
            unset($input['image']);
            $input['slug'] = str_slug($input['title'], '-');
            $input['datemodified'] = date('Y-m-d');
            Post::updatePost($input, $id);
            $request->session()->flash('status', 'Edit Post successful ! ' . $input['title']);
            return redirect('admin/post/edit/' . $id);
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $data['sidebar'] = Menu::getMenu('post', 'edit');
        $post = POST::find($id);
        $data['request'] = $request;

        if ($request->isMethod('post')) {
            return $this->do_edit($request, $id);
        }
        $data['post'] = $post;
        return view('admin::post.edit', $data);

    }


    public function destroy(Request $request, $id)
    {
          $post = POST::find($id);
        $data['sidebar'] = Menu::getMenu('post', '');
        $data['post'] = $post;
        if($request->isMethod('post')){
            $post = Post::find($id);
            Post::destroy($id);
            return redirect('admin/post')->with('status', 'delete ' . $post->title . ' successful');
        }
        return view('admin::post.delete', $data);
    }

    public function __construct()
    {
        //$this->middleware('auth');
    }

}