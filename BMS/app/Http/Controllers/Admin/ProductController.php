<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use App\Models\Post;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Intervention\Image\Facades\Image;
use Modules\Admin\data\Menu;
use Validator;
class ProductController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $status = [
        'draft' => 'Draft',
        'publish' => 'Publish'
    ];
    public function index(Request $request)
    {
        $data['sidebar'] = Menu::getMenu('category', 'index');
        $products = Post::where('object_type', 'product')
            ->paginate(10);
        $data['products'] = $products;
        $data['request'] = $request;
        return view('_admin.product.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

        if ($request->isMethod('post')) {
            return $this->doCreate($request);
        }
        $data['sidebar'] = Menu::getMenu('post', 'create');
        $data['categories'] = Category::where('object_type', 'category')->lists('title', 'id');
        $data['status'] = $this->status;
        return view('_admin.product.create', $data);
    }


    function doCreate($request)
    {

        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'excerpt' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('admin/product/create')
                ->withErrors($validator)
                ->withInput();
        }
        $post = new Post();
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            // echo $file->getClientOriginalName();die;
            $img = Image::make($file)->resize(500, 500);
            $img->save('public/media/images/' . $file->getClientOriginalName());
            $post->thumbnail = $file->getClientOriginalName();
        }
        if($request->salestate == 1){
            $post->salevalue = $request->salevalue;
            $post->saletype = $request->saletype;
        }

        $post->freeship = $request->freeship == 1 ? $request->freeship : 0;
        $post->title = $request->title;
        $post->object_type = 'product';
        $post->category_id = $request->categoryid;
        $post->slug = str_slug($request->title, '-');
        $post->excerpt = $request->excerpt;
        $post->quantity = $request->quantity;
        $post->description  = $request->description;
        $post->save();


        $request->session()->flash('status', 'Add new product success!' . $request->title);
        return redirect('admin/product/edit/' . $post->id);
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->isMethod('post')) {
            return $this->do_edit($request, $id);
        }

        $data['sidebar'] = Menu::getMenu('product', 'edit');
        $post = Post::find($id);
        $data['request'] = $request;
        $data['post'] = $post;

        $data['status'] = $this->status;
        $data['categories'] =Category::where('object_type', 'category')->lists('title', 'id');
        return view('_admin.product.edit', $data);
    }
    function do_edit($request, $id)
    {

        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'excerpt' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('admin/product/create')
                ->withErrors($validator)
                ->withInput();
        }
        $post = Post::find($id);
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            // echo $file->getClientOriginalName();die;
            $img = Image::make($file)->resize(500, 500);
            $img->save('public/media/images/' . $file->getClientOriginalName());
            $post->thumbnail= $file->getClientOriginalName();
        }

        if($request->salestate == 1){
            $post->salevalue = $request->salevalue;
            $post->saletype = $request->saletype;
        }else{
            $post->salevalue = 0;
            $post->saletype = '';
        }

        $post->freeship = $request->freeship == 1 ? $request->freeship : 0;
        $post->status = $request->status;
        $post->title = $request->title;
        $post->category_id = $request->categoryid;
        $post->slug = str_slug($request->title, '-');
        $post->excerpt = $request->excerpt;
        $post->quantity = $request->quantity;
        $post->description  = $request->description;
        $post->save();


        $request->session()->flash('status', 'edit product success!' . $request->title);
        return redirect('admin/product/edit/' . $post->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $post = Post::find($id);
        $data['sidebar'] = Menu::getMenu('category', '');
        $data['post'] = $post;
        if($request->isMethod('post')){
            $post = Post::find($id);
            Post::destroy($id);
            return redirect('admin/post')->with('status', 'delete ' . $post->title . ' successful');
        }
        return view('_admin.product.delete', $data);
    }
}
