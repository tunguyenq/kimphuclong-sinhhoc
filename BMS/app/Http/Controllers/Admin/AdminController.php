<?php namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Auth;
use Illuminate\Http\Request;
use Modules\Admin\data\Menu;
use Intervention\Image\ImageManagerStatic as Image;
use App\User;
use Validator;
use Modules\Admin\data\DataValidate;
class AdminController extends Controller {




	public function login(Request $request){
		if($request->isMethod('post')){
			return $this->do_login($request);
		}
		$data['request'] = $request;
		return view('_admin.admin.login', $data);
	}
	public function index(Request $request)
	{

	}
	function do_login($request){
		$validator = Validator::make($request->all(),[
		'email' => 'required|email',
			'password' => 'required'
		],DataValidate::getMessageError('login'));
		if($validator->fails()){
			return redirect('admin/')
				->withErrors($validator)
				->withInput();
		}
		$input = $request->all();
		if (Auth::attempt(['email' => $input['email'], 'password' => $input['password']])) {
			return redirect('admin/post');
		}
		else{
			return redirect('admin/')->with(['status' => 'The email and password you entered don\'t match.']);
		}
		return redirect('admin/');
	}

	public function profile(Request $request, $id){

		if($request->isMethod('post')){
			return $this->updateProfile($request, $id);
		}
		$data['sidebar'] = Menu::getMenu('admin', '');
		return view('_admin.admin.profile', $data);
	}
	public function setting(Request $request, $id){

		if($request->isMethod('post')){
			return $this->updateProfile($request, $id);
		}
		$data['sidebar'] = Menu::getMenu('admin', '');
		return view('_admin.admin.setting', $data);
	}

	function updateProfile($request, $id){

		if($request->hasFile('image')){
			$file = $request->file('image');
			$img = Image::make($file)->save('public/media/userimage/' . Auth::user()->email . '.jpg');

		}
		$user = User::find($id);
        $user->thumbnail = Auth::user()->email . '.jpg';
		$user->email = $request->email;
		$user->name = $request->name;
		$user->phone = $request->phone;
		$user->save();
		return redirect('admin/profile/' . $id);
	}
	public function demo(){
		return view('_admin.admin.demo');
	}
	
}