<?php

namespace App\Http\Controllers\Admin;
use App\Models\Post;
use Illuminate\Http\Request;
use Intervention\Image\Facades\Image;
use App\Http\Requests;
use App\Http\Controllers\Controller;
use Modules\Admin\data\Menu;
use Validator;

class SliderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public $status = [
        'draft' => 'Draft',
        'publish' => 'Publish'
    ];
    public function index(Request $request)
    {

        $data['sidebar'] = Menu::getMenu('slider', 'index');
        $sliders = Post::where('object_type', 'slider')
            ->paginate(10);
        $data['sliders'] = $sliders;
        $data['request'] = $request;
        return view('_admin.slider.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if ($request->isMethod('post')) {
            return $this->doCreate($request);
        }
        $data['sidebar'] = Menu::getMenu('slider', 'create');
        $data['status'] = $this->status;
        return view('_admin.slider.create', $data);
    }

    function doCreate($request)
    {

        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'excerpt' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('admin/slider/create')
                ->withErrors($validator)
                ->withInput();
        }
        $input = $request->all();
        $input['thumbnail'] = '';
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            // echo $file->getClientOriginalName();die;
            $img = Image::make($file)->resize(900, 400);
            $img->save('public/media/images/' . str_slug($file->getClientOriginalName()));
            $input['thumbnail'] = str_slug( $file->getClientOriginalName());
        }
        $post = new Post();
        $post->title = $request->title;
        $post->object_type = 'slider';
        $post->thumbnail = $input['thumbnail'];
        $post->excerpt = $request->excerpt;
        $post->save();


        $request->session()->flash('status', 'Add new product sucess!' . $request->title);
        return redirect('admin/slider/edit/' . $post->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->isMethod('post')) {
            return $this->do_edit($request, $id);
        }

        $data['sidebar'] = Menu::getMenu('slider', 'edit');
        $post = Post::find($id);
        $data['request'] = $request;
        $data['post'] = $post;
        $data['status'] = $this->status;
        return view('_admin.slider.edit', $data);
    }

    function do_edit($request, $id)
    {
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
            'excerpt' => 'required',
        ]);

        if ($validator->fails()) {
            return redirect('admin/product/create')
                ->withErrors($validator)
                ->withInput();
        }
        $post = Post::find($id);
        if ($request->hasFile('image')) {
            $file = $request->file('image');
            // echo $file->getClientOriginalName();die;
            $img = Image::make($file)->resize(900, 400);
            $img->save('public/media/images/' . str_slug( $file->getClientOriginalName()));
            $post->thumbnail = str_slug( $file->getClientOriginalName());

        }
        $post->status = $request->status;
        $post->title = $request->title;
        $post->excerpt = $request->excerpt;
        $post->slug = str_slug($request->title, '-');
        $post->save();


        $request->session()->flash('status', 'edit slider success!' . $request->title);
        return redirect('admin/slider/edit/' . $post->id);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $post = Post::find($id);
        $data['sidebar'] = Menu::getMenu('slider', '');
        $data['post'] = $post;
        if($request->isMethod('post')){
            $post = Post::find($id);
            Post::destroy($id);
            return redirect('admin/slider')->with('status', 'delete ' . $post->title . ' successful');
        }
        return view('_admin.slider.delete', $data);
    }
}
