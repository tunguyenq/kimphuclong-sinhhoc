<?php

namespace App\Http\Controllers\Admin;

use App\Models\Category;
use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;
use Modules\Admin\data\Menu;
use Validator;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {

        $data['sidebar'] = Menu::getMenu('category', 'index');
        $data['categories'] = Category::where('object_type', 'category')
            ->paginate(10);
        $data['request'] = $request;
        return view('_admin.category.index', $data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $data['sidebar'] = Menu::getMenu('category', 'create');
        if ($request->isMethod('post')) {
            return $this->doCreate($request);
        }

        $data['categories'] = Category::where('object_type', 'category')
            ->lists('title', 'id');
        return view('_admin.category.create', $data);
    }

    function doCreate($request)
    {

        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
        ]);

        if ($validator->fails()) {
            return redirect('admin/category/create')
                ->withErrors($validator)
                ->withInput();
        }
        $input = $request->all();
        unset($input['_token']);
        unset($input['image']);
        $input['slug'] = str_slug($input['title'], '-');
        $input['created_at'] = date('Y-m-d');
        $category = new Category();
        $category->title = $input['title'];
        $category->object_type = 'category';
        $category->parent_id = $request->categoryid;
        $category->slug = str_slug($request->title, '-');
        $category->save();

        $request->session()->flash('status', 'Add new category success!' . $request->title);
        return redirect('admin/category/edit/' . $category->id);
    }

    /**
     * Display the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request $request
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if ($request->isMethod('post')) {
            return $this->do_edit($request, $id);
        }
        $data['sidebar'] = Menu::getMenu('category', 'edit');
        $category = Category::find($id);
        $data['request'] = $request;
        $data['post'] = $category;

        return view('_admin.category.edit', $data);
    }

    function do_edit($request, $id)
    {
        //validate data
        $validator = Validator::make($request->all(), [
            'title' => 'required|max:255',
//            'excerpt' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect('admin/category/edit/' . $id)
                ->withErrors($validator)
                ->withInput();
        } else {

            $category = Category::find($id);
            $category->title = $request->title;
            $category->slug = str_slug($request->title, '-');
            $category->save();
            $request->session()->flash('status', 'Edit Post successful ! ' . $request->title);
            return redirect('admin/category/edit/' . $id);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id, Request $request)
    {
        $post = Category::find($id);
        $data['sidebar'] = Menu::getMenu('category', '');
        $data['post'] = $post;
        if ($request->isMethod('post')) {
            $post = Category::find($id);
            Category::destroy($id);
            return redirect('admin/category')->with('status', 'delete ' . $post->title . ' successful');
        }
        return view('_admin.category.delete', $data);
    }
}
