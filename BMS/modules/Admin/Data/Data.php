<?php
namespace Modules\Admin\data;
/**
 * Created by PhpStorm.
 * User: tunguyenex
 * Date: 2/11/2016
 * Time: 12:30 PM
 */
class Data{
	public static function get($strFileDotField){
		$arr = explode('.', $strFileDotField);
        $file = include $arr[0]. '.php';
        unset($arr[0]);
        foreach($arr as $item){
            $file = $file[$item];
        }
        return $file;
	}
}