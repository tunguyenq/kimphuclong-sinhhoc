<?php
/**
 * Created by PhpStorm.
 * User: tunguyenex
 * Date: 2/2/2016
 * Time: 12:46 AM
 */
return [
//    'post' => [
//        'name' => 'Post',
//        'icon' => 'fa fa-table'
//    ],
    'page' => [
        'name' => 'Page',
        'icon' => 'fa fa-newspaper-o'
    ],
    'news' => [
        'name' => 'Tin tức',
        'icon' => 'fa fa-newspaper-o'
    ],
    'category' => [
        'name' => 'Category',
        'icon' => 'fa fa-gift'
    ],  
    'product' => [
        'name' => 'Product',
        'icon' => 'fa fa-gift'
    ],
    'video' => [
        'name' => 'Video',
        'icon' => 'fa fa-video-camera'
    ],
    'slider' => [
        'name' => 'Slider',
        'icon' => 'fa fa-sliders'
    ],
//    'news' => [
//        'name' => 'News',
//        'icon' => 'fa fa-table'
//    ],
    'user' => [
        'name' => 'User',
        'icon' => 'fa fa-table',
        'roles' => [
            'user' => 'User',
            'admin' => 'Admin'
        ],
        'status' => [
            'deactive' => 'Deacitve',
            'active' => 'Acitive'
        ]
    ]

];