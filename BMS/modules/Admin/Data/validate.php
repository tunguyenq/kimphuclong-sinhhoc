<?php
/**
 * Created by PhpStorm.
 * User: tunguyenex
 * Date: 2/1/2016
 * Time: 11:50 PM
 */
return [
    'post' => [
        'title.required' => 'A title is required',
        'excerpt.required'  => 'A excerpt is required',
    ],
    'news' => [],
    'user' => [
        'password.required' => 'Password is required',
        'email.required' => 'The email required',
        'email.unique' => 'the email must unique',
        'password.confirmed' => 'The password confirmation does not match.'
    ],
    'login' => [
        'password.required' => 'Password is required',
        'email.required' => 'The email required',
    ]
];