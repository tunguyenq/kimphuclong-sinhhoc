<header class="header-container header-type-default header-navbar-default">
    <div class="topbar">
        <div class="container">
            <div class="row">
                <div class="col-md-5 col-sm-6">
                    <div class="left-topbar">
                        <div class="topbar-info">
                            <a href="#"><i class="fa fa-phone"></i> Hotline: 098 6393160</a>
                            <a href="#">
                                <i class="fa fa-envelope-o"></i>
                                <span><span class="__cf_email__" >Email: info@kimphuclong.com.vn</span></span>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="col-md-7 col-sm-6">
                </div>
            </div>
        </div>
    </div>
    <div class="navbar-container">
        <div class="navbar navbar-default navbar-scroll-fixed">
            <div class="navbar-default-wrap">
                <div class="container">
                    <div class="navbar-wrap">
                        <div class="navbar-header">
                            <button data-target=".primary-navbar-collapse" data-toggle="collapse" type="button" class="navbar-toggle">
                                <span class="sr-only">Toggle navigation</span>
                                <span class="icon-bar bar-top"></span>
                                <span class="icon-bar bar-middle"></span>
                                <span class="icon-bar bar-bottom"></span>
                            </button>
                            <a class="cart-icon-mobile" href="#">
                                <i class="elegant_icon_bag"></i><span>0</span>
                            </a>
                            <a class="navbar-brand" title="AirSlice" href="<?php echo e(url('')); ?>">
                                <img class="logo" alt="AirSlice" src='<?php echo e(URL::asset('public/media/images/logo.jpg')); ?>'/>
                                <img class="logo-fixed" alt="AirSlice" src='<?php echo e(URL::asset('public/media/images/logo.jpg')); ?>'>
                                <img class="logo-mobile" alt="AirSlice" src='<?php echo e(URL::asset('public/media/images/logo.jpg')); ?>'>
                            </a>
                        </div>
                        <nav class="collapse navbar-collapse primary-navbar-collapse">
                            <ul class="nav navbar-nav primary-nav">
                                <li class="menu-item-has-children megamenu dropdown active">
                                    <a title="Home" href="<?php echo e(url('')); ?>" class="dropdown-hover">
                                        <i class="navicon elegant_icon_house_alt"></i>
                                        <span class="underline">Trang chủ</span> <span class="caret"></span>
                                    </a>
                                </li>
                                <li class="menu-item-has-children megamenu megamenu-fullwidth dropdown">
                                    <a title="Features" href="<?php echo e(url('/gioi-thieu')); ?>" class="dropdown-hover">
                                        <i class="navicon elegant_icon_lightbulb_alt "></i>
                                        <span class="underline">Giới thiệu</span> <span class="caret"></span>
                                    </a>

                                </li>
                                <li class="menu-item-has-children dropdown">
                                    <a title="Blog" href="#" class="dropdown-hover">
                                        <i class="navicon elegant_icon_document_alt "></i>
                                        <span class="underline">Sản phẩm</span> <span class="caret"></span>
                                    </a>
                                    <ul class="dropdown-menu">
                                        <?php foreach($categories as $category): ?>
                                            <li><a href="<?php echo e(url('sanpham/' . $category->slug. '/' . $category->id)); ?>"><?php echo e($category->title); ?></a></li>
                                            <?php endforeach; ?>
                                    </ul>
                                </li>
                                <li class="menu-item-has-children dropdown">
                                    <a title="Portfolio" href="#" class="dropdown-hover">
                                        <i class="navicon elegant_icon_archive_alt"></i>
                                        <span class="underline">Video sản phẩm</span> <span class="caret"></span>
                                    </a>
                                </li>
                                <li class="menu-item-has-children dropdown">
                                    <a title="Portfolio" href="<?php echo e(url('/tuyen-dung')); ?>" class="dropdown-hover">
                                        <i class="navicon elegant_icon_archive_alt"></i>
                                        <span class="underline">Tuyển dụng</span> <span class="caret"></span>
                                    </a>
                                </li>
                                <li class="menu-item-has-children dropdown">
                                    <a title="Portfolio" href="<?php echo e(url('/dai-ly')); ?>" class="dropdown-hover">
                                        <i class="navicon elegant_icon_archive_alt"></i>
                                        <span class="underline">Đại lý</span> <span class="caret"></span>
                                    </a>
                                </li>
                                <li class="menu-item-has-children dropdown">
                                    <a title="Portfolio" href="<?php echo e(url('/khuyen-mai')); ?>" class="dropdown-hover">
                                        <i class="navicon elegant_icon_archive_alt"></i>
                                        <span class="underline">Khuyến mãi</span> <span class="caret"></span>
                                    </a>
                                </li>
                                <li class="menu-item-has-children megamenu megamenu-fullwidth dropdown">
                                    <a title="Shortcodes" href="<?php echo e(url('/lien-he')); ?>" class="dropdown-hover">
                                        <i class="navicon elegant_icon_box-checked"></i>
                                        <span class="underline">Liên hệ</span> <span class="caret"></span>
                                    </a>
                                </li>
                                <li class="navbar-search">
                                    <a class="navbar-search-button" href="#">
                                        <i class="fa fa-search"></i>
                                    </a>
                                    <div class="search-form-wrap show-popup hide">
                                        <form class="searchform">
                                            <input type="search" class="searchinput" placeholder="Search..."/>
                                            <input type="submit" class="searchsubmit hidden" name="submit" value="Search"/>
                                        </form>
                                    </div>
                                </li>
                            </ul>
                        </nav>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>
<div class="heading-container ">
    <div class="container heading-standar">
        <div class="heading-wrap">
            <div class="page-breadcrumb clearfix">
                <div class="pull-left">
                    <ul class="breadcrumb">
                        <li>
                            <a href="<?php echo e(url('')); ?>" class="home"><span>Trang chủ</span></a>
                        </li>
                        <li><span>Kim Phúc Long</span></li>
                    </ul>
                </div>
                <div class="heading-info pull-right">
                    <em>Hotline:</em>&nbsp;&nbsp;
                    <i class="fa fa-phone"></i> 098 6393160&nbsp;&nbsp;&nbsp;
                    <a href="#">
                        <i class="fa fa-envelope-o"></i>
                        <span><span class="__cf_email__" data-cfemail="">Email: info@kimphuclong.com.vn</span></span>
                    </a>
                </div>
            </div>
            <div class="page-title">
                <h1>Kim Phúc Long</h1>
            </div>
        </div>
    </div>
</div>