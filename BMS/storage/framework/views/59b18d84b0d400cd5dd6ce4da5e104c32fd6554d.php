<?php $__env->startSection('content'); ?>

    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <h1>
                Post page
                <small>it all starts here</small>
            </h1>
            <ol class="breadcrumb">
                <li><a href="#"><i class="fa fa-dashboard"></i> Home</a></li>
                <li><a href="#">Examples</a></li>
                <li class="active">Blank page</li>
            </ol>
        </section>

        <!-- Main content -->
        <section class="content">
            <?php if($request->session()->has('status')): ?>
                <div class="alert alert-danger"><?php echo e($request->session()->pull('status', '')); ?>

                    <button aria-hidden="true" data-dismiss="alert" class="close" type="button">×</button>
                </div>
                <?php endif; ?>
                        <!-- Default box -->
                <div class="row">
                    <div class="col-xs-12">
                        <div class="box">
                            <div class="box-header">
                                <h3 class="box-title">Responsive Hover Table</h3>

                                    <div class="box-tools">
                                        <form action="<?php echo e(url('admin/post')); ?>" method="get">

                                        <div style="width: 150px;" class="input-group">
                                            <input type="text" placeholder="Search"
                                                   class="form-control input-sm pull-right" name="keyword">
                                            <div class="input-group-btn">
                                                <button class="btn btn-sm btn-default"><i class="fa fa-search"></i>
                                                </button>
                                            </div>
                                        </div>
                                        </form>

                                    </div>
                            </div><!-- /.box-header -->
                            <div class="box-body no-padding">
                                <table class="table table-hover">

                                    <div class="container">
                                    </div>
                                    <tbody>
                                    <tr>
                                        <th>ID</th>
                                        <th>Title</th>
                                        <th>Excerpt</th>
                                        <th>Status</th>
                                        <th>Option</th>
                                    </tr>

                                    <?php foreach($posts as $post): ?>
                                        <tr>
                                            <td><?php echo e($post->id); ?></td>
                                            <td><?php echo e($post->title); ?></td>
                                            <td><?php echo e($post->updated_at); ?></td>
                                            <td><span class="label label-success">Approved</span></td>
                                            <td width="400"><?php echo e($post->excerpt); ?></td>
                                            <td>
                                                <div class="btn-group">
                                                    <button class="btn btn-success btn-sm" type="button">Action</button>
                                                    <button data-toggle="dropdown"
                                                            class="btn btn-success btn-sm dropdown-toggle" type="button"
                                                            aria-expanded="false">
                                                        <span class="caret"></span>
                                                        <span class="sr-only">Toggle Dropdown</span>
                                                    </button>
                                                    <ul role="menu" class="dropdown-menu">
                                                        <li><a href="<?php echo e(url('admin/post/show/'. $post->id)); ?>">View</a>
                                                        </li>
                                                        <li><a href="<?php echo e(url('admin/post/edit/'. $post->id)); ?>">Edit</a>
                                                        </li>
                                                        <li>
                                                            <a href="<?php echo e(url('admin/post/delete/' . $post->id)); ?>">Delete</a>
                                                        </li>

                                                    </ul>
                                                </div>
                                            </td>
                                        </tr>
                                    <?php endforeach; ?>

                                    </tbody>
                                </table>
                            </div><!-- /.box-body -->
                            <div class="box-footer clearfix ">
                                <div class="pull-right">
                                 <?php echo e($posts->appends(['keyword' => $request->keyword])->links()); ?>

                                </div>
                            </div>
                        </div><!-- /.box -->
                    </div>
                </div>

        </section><!-- /.content -->
    </div>

<?php $__env->stopSection(); ?>
<?php echo $__env->make('_admin.layouts.main', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>