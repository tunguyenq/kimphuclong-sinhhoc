<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Công ty TNHH Kim Phúc Long</title>
    <link rel='stylesheet' href='<?php echo e(URL::asset('public/front-end/css/settings.css')); ?>' type='text/css' media='all'/>
    <link rel='stylesheet' href='<?php echo e(URL::asset('public/front-end/css/elegant-icon.css')); ?>' type='text/css' media='all'/>
    <link rel='stylesheet' href='<?php echo e(URL::asset('public/front-end/css/font-awesome.min.css')); ?>' type='text/css' media='all'/>
    <link rel='stylesheet' href='<?php echo e(URL::asset('public/front-end/css/style.css')); ?>' type='text/css' media='all'/>
    <link rel='stylesheet' href='<?php echo e(URL::asset('public/front-end/css/shop.css')); ?>' type='text/css' media='all'/>
    <link rel='stylesheet'  href='<?php echo e(URL::asset('public/front-end/css/preloader.css')); ?>' type='text/css' media='all'/>
    <link rel='stylesheet'  href='<?php echo e(URL::asset('public/front-end/css/magnific-popup.css')); ?>' type='text/css' media='all'/>
    <link rel='stylesheet'  href='<?php echo e(URL::asset('public/front-end/css/skin-selector.css')); ?>' type='text/css' media='all'/>
    <link rel='stylesheet'  href='<?php echo e(URL::asset('public/front-end/css/kpl.css')); ?>' type='text/css' media='all'/>

</head>
<body>
<div id="fb-root"></div>
<script>(function(d, s, id) {
        var js, fjs = d.getElementsByTagName(s)[0];
        if (d.getElementById(id)) return;
        js = d.createElement(s); js.id = id;
        js.src = "//connect.facebook.net/en_US/sdk.js#xfbml=1&version=v2.7&appId=1852601481627653";
        fjs.parentNode.insertBefore(js, fjs);
    }(document, 'script', 'facebook-jssdk'));</script>



<div id="preloader">
    <img class="preloader__logo" src='<?php echo e(URL::asset('public/media/images/logo.jpg')); ?>' alt=""/>
    <div class="preloader__progress">
        <svg width="60px" height="60px" viewBox="0 0 80 80" xmlns="http://www.w3.org/2000/svg">
            <path class="preloader__progress-circlebg" fill="none" stroke="#dddddd" stroke-width="4"
                  stroke-linecap="round"
                  d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z"/>
            <path id='preloader__progress-circle' fill="none" stroke="#57bb8a" stroke-width="4" stroke-linecap="round"
                  stroke-dashoffset="192.61" stroke-dasharray="192.61 192.61"
                  d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z"/>
        </svg>
    </div>
</div>
<div id="wrapper" class="boxed-wrap">
    <div class="morphsearch" id="morphsearch">
        <form>
            <input type="search" name="s" placeholder="Search..." class="morphsearch-input">
            <button type="submit" class="morphsearch-submit"></button>
        </form>
        <span class="morphsearch-close"></span>
    </div>
    <?php echo $__env->make('layouts.header', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
    <?php echo $__env->yieldContent('content'); ?>
    <?php echo $__env->make('layouts.footer', array_except(get_defined_vars(), array('__data', '__path')))->render(); ?>
</div>





<script type='text/javascript' src='<?php echo e(URL::asset('public/front-end/js/jquery.js')); ?>'></script>
<script type='text/javascript' src='<?php echo e(URL::asset('public/front-end/js/jquery-migrate.min.js')); ?>'></script>
<script type='text/javascript' src='<?php echo e(URL::asset('public/front-end/js/jquery.themepunch.tools.min.js')); ?>'></script>
<script type='text/javascript' src='<?php echo e(URL::asset('public/front-end/js/jquery.themepunch.revolution.min.js')); ?>'></script>
<script type='text/javascript' src='<?php echo e(URL::asset('public/front-end/js/preloader.min.js')); ?>'></script>
<script type='text/javascript' src='<?php echo e(URL::asset('public/front-end/js/easing.min.js')); ?>'></script>
<script type='text/javascript' src='<?php echo e(URL::asset('public/front-end/js/imagesloaded.pkgd.min.js')); ?>'></script>
<script type='text/javascript' src='<?php echo e(URL::asset('public/front-end/js/bootstrap.min.js')); ?>'></script>
<script type='text/javascript' src='<?php echo e(URL::asset('public/front-end/js/superfish-1.7.4.min.js')); ?>'></script>
<script type='text/javascript' src='<?php echo e(URL::asset('public/front-end/js/jquery.appear.min.js')); ?>'></script>
<script type='text/javascript' src='<?php echo e(URL::asset('public/front-end/js/script.js')); ?>'></script>
<script type='text/javascript' src='<?php echo e(URL::asset('public/front-end/js/jquery.parallax.js')); ?>'></script>
<script type='text/javascript' src='<?php echo e(URL::asset('public/front-end/js/jquery.countTo.min.js')); ?>'></script>
<script type='text/javascript' src='<?php echo e(URL::asset('public/front-end/js/isotope.pkgd.min.js')); ?>'></script>
<script type='text/javascript' src='<?php echo e(URL::asset('public/front-end/js/jquery.magnific-popup.min.js')); ?>'></script>
<script type='text/javascript' src='<?php echo e(URL::asset('public/front-end/js/jquery.touchSwipe.min.js')); ?>'></script>
<script type='text/javascript' src='<?php echo e(URL::asset('public/front-end/js/jquery.carouFredSel.min.js')); ?>'></script>
<script type='text/javascript' src='<?php echo e(URL::asset('public/front-end/js/skin-selector.js')); ?>'></script>
<script type="text/javascript">

    /******************************************
     -    PREPARE PLACEHOLDER FOR SLIDER    -
     ******************************************/


    var setREVStartSize = function () {
        var tpopt = new Object();
        tpopt.startwidth = 500;
        tpopt.startheight = 650;
        tpopt.container = jQuery('#rev_slider_2_1');
        tpopt.fullScreen = "off";
        tpopt.forceFullWidth = "off";
    };

    /* CALL PLACEHOLDER */
    setREVStartSize();

    var tpj = jQuery;
    tpj.noConflict();
    var revapi2;
    tpj(document).ready(function () {

        if (tpj('#rev_slider_2_1').revolution == undefined) {
            revslider_showDoubleJqueryError('#rev_slider_2_1');
        } else {
            revapi2 = tpj('#rev_slider_2_1').show().revolution(
                    {
                        dottedOverlay: "none",
                        delay: 9000,
                        startwidth: 500,
                        startheight: 350,
                        hideThumbs: 200,

                        thumbWidth: 100,
                        thumbHeight: 50,
                        thumbAmount: 2,


                        simplifyAll: "off",

                        navigationType: "bullet",
                        navigationArrows: "solo",
                        navigationStyle: "round",

                        touchenabled: "on",
                        onHoverStop: "on",
                        nextSlideOnWindowFocus: "off",

                        swipe_threshold: 0.7,
                        swipe_min_touches: 1,
                        drag_block_vertical: false,


                        keyboardNavigation: "off",

                        navigationHAlign: "center",
                        navigationVAlign: "bottom",
                        navigationHOffset: 0,
                        navigationVOffset: 20,

                        soloArrowLeftHalign: "left",
                        soloArrowLeftValign: "center",
                        soloArrowLeftHOffset: 20,
                        soloArrowLeftVOffset: 0,

                        soloArrowRightHalign: "right",
                        soloArrowRightValign: "center",
                        soloArrowRightHOffset: 20,
                        soloArrowRightVOffset: 0,

                        shadow: 0,
                        fullWidth: "on",
                        fullScreen: "off",

                        spinner: "spinner3",

                        stopLoop: "off",
                        stopAfterLoops: -1,
                        stopAtSlide: -1,

                        shuffle: "off",

                        autoHeight: "off",
                        forceFullWidth: "off",


                        hideTimerBar: "on",
                        hideThumbsOnMobile: "off",
                        hideNavDelayOnMobile: 1500,
                        hideBulletsOnMobile: "off",
                        hideArrowsOnMobile: "off",
                        hideThumbsUnderResolution: 0,

                        hideSliderAtLimit: 0,
                        hideCaptionAtLimit: 0,
                        hideAllCaptionAtLilmit: 0,
                        startWithSlide: 0
                    });


        }
    });
    /*ready*/

</script>
</body>
</html>

