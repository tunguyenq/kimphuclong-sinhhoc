<div class="main-sidebar">
    <div class="row">
        <div class="col-sm-12">
            <iframe width="500" height="680" src="https://www.youtube.com/embed/OTmwZgduKho"
                    frameborder="0" allowfullscreen></iframe>
        </div>
    </div>
    <div class="widget widget_text">
        <div class="textwidget">
            <div class="tabbable tabs-primary tabs-top">
                <ul class="nav nav-tabs" role="tablist">
                    <li>
                        <a href="#pane-2" role="tab" data-toggle="tab">Sản phẩm gần đây</a>
                    </li>
                </ul>
                <div class="tab-content">

                    <div class="tab-pane fade active in" id="pane-2">
                        <div class="widget widget-post-thumbnail">
                            <ul class="posts-thumbnail-list">
                                <?php foreach($products_sidebar as $product): ?>
                                    <li>
                                        <div class="posts-thumbnail-image">
                                            <a href="#">
                                                <img width="600" height="450"
                                                     src='<?php echo e(URL::asset('public/media/images/' . $product->thumbnail)); ?>'
                                                     alt=""/>
                                            </a>
                                        </div>
                                        <div class="posts-thumbnail-content">
                                            <time datetime="2014-10-12T15:33:24+00:00">
                                                <?php echo e(date('d-m-Y', strtotime($product->created_at))); ?>

                                            </time>
                                            <span class="comment-count">
                                                                                <a href="#">
                                                                                    <i class="fa fa-comments-o"></i>4
                                                                                </a>
                                                                            </span>
                                            <h4><a href="#"><?php echo e($product->title); ?></a></h4>
                                        </div>
                                    </li>
                                <?php endforeach; ?>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="widget widget_categories">
        <h4 class="widget-title"><span>Loại sản phẩm</span></h4>

        <ul>
            <?php foreach($categories as $category): ?>
                <li><a href="<?php echo e(url('sanpham/' . $category->slug . '/' . $category->id)); ?>"><?php echo e($category->title); ?></a></li>

            <?php endforeach; ?>
        </ul>
    </div>
    <div class="widget widget_tag_cloud hidden">
        <h4 class="widget-title"><span>Tags</span></h4>
        <div class="tagcloud">
            <a href="#">blog</a><a href="#">gallery</a><a href="#">image</a><a href="#">post</a><a
                    href="#">standard</a><a
                    href="#">video</a><a href="#">vimeo</a><a href="#">youtube</a>
        </div>
    </div>
    <div class="widget widget_text">
        <div class="textwidget">
            <div class="fb-page" data-href="https://www.facebook.com/newvisionvn/"
                 data-tabs="timeline" data-small-header="false" data-adapt-container-width="true"
                 data-hide-cover="false" data-show-facepile="true">
                <blockquote cite="https://www.facebook.com/newvisionvn/"
                            class="fb-xfbml-parse-ignore"><a
                            href="https://www.facebook.com/newvisionvn/">I Dare</a></blockquote>
            </div>
        </div>
    </div>
</div>
