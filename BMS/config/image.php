<?php

return array(

    /*
    |--------------------------------------------------------------------------
    | Image Driver
    |--------------------------------------------------------------------------
    |
    | Intervention Image supports "GD Library" and "Imagick" to process images
    | internally. You may choose one of them according to your PHP
    | configuration. By default PHP's "GD Library" implementation is used.
    |
    | Supported: "gd", "imagick"
    |
    */

    'driver' => 'gd',
    'path' => 'public/media/images/',
    'path-user' => 'public/media/userimage/',
    'templates' => array(
        'small' => ['w' => 120, 'h' => 90],
        'medium' =>['w' => 240, 'h' => 180],
        'large' => ['w' => 480, 'h' => 360],
        '500|500' => ['w' => 500, 'h' => 500],
    )


);
